#pragma once

enum TILETYPE
{
	FLOOR = 0,
	WALL,
	OPENDOOR,
	CLOSEDOOR
};