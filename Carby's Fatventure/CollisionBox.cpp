#include "CollisionBox.h"


void hitBox::setVelocity(float velX, float velY)
{
	velocityX = velX;
	velocityY = velY;
}

void hitBox::setLengths(float lengthX, float lengthY)
{
	lengths.x = lengthX;
	lengths.y = lengthY;
}

void hitBox::setScale(float setscale) // also sets lengths accordingly
{
	scale = setscale;
	lengths.x *= scale;
	lengths.y *= scale;
}

void hitBox::setPositionCoord(float x, float y)
{
	positionX = x;
	positionY = y;
}

bool hitBox::chkAABB(hitBox &obj2)
{
	// check collision at x-axis
	bool collisionX = max.x >= obj2.min.x &&
		obj2.max.x >= min.x;
	// check collision at y-axis
	bool collisionY = max.y >= obj2.min.y &&
		obj2.max.y >= min.y;
	// Collision only if on both axes
	return collisionX && collisionY;
}

AEVec2 hitBox::getcurrPos()
{
	AEVec2 thisPos;

	thisPos.x = positionX;
	thisPos.y = positionY;

	return thisPos;
}

void hitBox::minmaxUpdate()
{
	min.x = positionX - lengths.x;
	min.y = positionY - lengths.y;
	max.x = positionX + lengths.x;
	max.y = positionY + lengths.y;
}

hitBox::hitBox()
{
	setLengths(32.0f, 32.0f);
	setScale(1.0f);
	setVelocity(0, 0);
	setPositionCoord(0.0f, 0.0f);

	minmaxUpdate();
}

hitBox::~hitBox()
{

}