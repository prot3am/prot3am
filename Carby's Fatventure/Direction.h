#pragma once

/*Start with Up as 1, and rotate clockwise*/
enum DIRECTION
{
	TOP = 1,
	TOPRIGHT,
	RIGHT,
	BTMRIGHT,
	BTM,
	BTMLEFT,
	LEFT,
	TOPLEFT
};