#pragma once
#include "ParticleEffType.h"
#include "ParticleEffect.h"
#include <AEGraphics.h>

class ParticleEffectSystem
{

public:
	ParticleEffectSystem();
	ParticleEffectSystem(float varDir, float varPos, float dur, float rate, int max, EFFECT_TYPE eff, float pSpd, float pAccel, float pDur, AEVec2 pDir, AEVec2 pos);

	void Update(float dt);
	void Draw();

	void TurnOff();
	void TurnOff(int i);
	void TurnOn();
	void TurnOn(int i);
	void setVarDir(float var);
	void setVarPos(float var);
	void setDur(float dur);
	void resetDurTimer();
	void setRate(float newSRate);
	void resetSpawnTimer();
	void setMaxParticleCount(int maxCount);
	void resetCurParticleCount();
	void setParticlePos(AEVec2 newPos);
	void setParticleEff(EFFECT_TYPE newEff);
	void setParticleSpd(float newSpd);
	void setParticleAccel(float newAccel);
	void setParticleDur(float newDur);
	void setParticleDir(AEVec2 newDir);


	~ParticleEffectSystem();

private:
	//For the particle activity
	bool isSpawning; //Determines if spawning is on or not
	bool isActive; //Determines if update is active or not
	bool isViewable; //Determines if particle system can be seen

	//For particle spawning
	float varianceDir; //Detemrines amount of variance. 0 is for no variance in particle direction, 1.0f for max variance (complete random)
	float variancePos; //Determiens amount of variance in position. Moves in a circle around the default position (direct length values)
	float duration; //Determines the duration of particle system
	float durTimer; //Timer to check for duration
	float spawnRate; //Determines rate of particle spawn. 1.0f means 1s before each spawn
	float spawnTimer; //Timer to check for spawn and to check with duration
	int particleMaxCount; //Determines maximum amount of particles
	int particleCurCount; //Current particle count in this particle effects system
	AEVec2 particlePos; //Determines position of particle spawns
	EFFECT_TYPE particleEffType; //Determines spawn type
	ParticleEffect* particles;

	//For the particles variables
	float particleSpd; //Determines velocity of particles
	float particleAccel; //Determines speed of particles
	float particleDur; //Determines the duration of particle
	//float particleTimer; This is not needed because timers are always init to 0
	AEVec2 particleDir; //Normal vector of the direction to move in
};

