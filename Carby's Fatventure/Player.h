#pragma once
#include "Entity.h"
#include "Slot.h"
#include "AEEngine.h"

class Player :
	public Entity
{
public:
	Player();
	Player(int HP, int maxHP, int attack, float speed, int defense, int mana,int maxmana);
	~Player();
	void InitRender();
	void Update(float dt);
	void Draw();
	void EnergyBarUpdate();
	void ChangeSkills(bool OldKey, bool NewKey);
	//bool PlayerUseSkill(Slot skillSlots[]);

protected:
	int maxEnergy;
	int energy;
	Slot skillSlots[5];

	float speedmod;

	unsigned int ticks;

	AEGfxTexture *PlayerTexture;
	AEGfxTexture *AbilityBar;
	AEGfxTexture *HealthIcon;
	AEGfxTexture *EnergyIcon;
	AEGfxVertexList *PlayerMesh;
	AEGfxVertexList *SlotBarMesh;
	AEGfxVertexList *ResourceMesh;
	AEGfxVertexList *ResourceMeshHalf;
	AEGfxVertexList *ResourceMeshThird;
};

