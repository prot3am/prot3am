#include "pch.h"
//#include "stdfx.h"

#include <fstream>
#include <string>
#include "Enemy.h"
#include "Node.h"
#include "ReadFile.h"
#include "Slot.h"

using namespace std;
/*
void Enemy::SaveEnemyToFile()
{
	string name;
	//ENEMYTYPE behaviour, DIRECTION direction;
	int HP = 0, Atk = 0, Spd = 0, Level = 0, Range = 0, Width = 0;
	float AtkCD = 0;

	name = enemy.name; //how to get the Name() function from the Enemy::Entity
	//HP = HP();
	//Atk = Atk();
	//Spd = Spd();
	behaviour = WhatYouNeedHere_1();
	direction = WhatYouNeedHere_2();
	Level = getLevel();
	Range = Range();
	Width = Width();
	AtkCD = AtkCD();

	//write to data file 
	ofstream SaveFile("Data/Save.txt", ofstream::app);
	SaveFile << "Name: " << name << "\n";
	SaveFile << "Atk: " << Atk << "\n";
	SaveFile << "Spd: " << Spd << "\n";
	SaveFile << "Behaviour: " << behaviour << "\n";
	SaveFile << "Direction: " << direction << "\n";
	SaveFile << "Level: " << Level << "\n";
	SaveFile << "Range: " << Range << "\n";
	SaveFile << "Width: " << Width << "\n";
	SaveFile << "AtkCD: " << AtkCD << "\n";

}
*/

void SaveMilstoneAchievement()
{
	//count how many enemies died
	//count damage done
	//count damage taken
	int EnemyKilled = 0, DamageDone = 20, DamageRecieved = 0, GoldRecieved = 0,
		UpgradesDone = 0, EnergyCost = 0;

	ofstream file("Data/milestones.txt", ofstream::trunc);

	file << "enemiesKilled: " << EnemyKilled << "\n";
	file << "DamageDone: " << DamageDone << "\n";
	file << "DamageRecieved: " << DamageRecieved << "\n";
	file << "GoldRecieved: " << GoldRecieved << "\n";
	file << "UpgradesDone: " << UpgradesDone << "\n";
	file << "EnergyCost: " << EnergyCost << "\n";
}

