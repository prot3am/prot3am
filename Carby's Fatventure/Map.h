#pragma once
#include "Room.h"
#include <random>

class Map
{
private:
	Room *rooms;//Array of pointers to Room Class 
	Room *currentRoom;
	int width;
	int height;
	unsigned int maxRooms;//Maximum number of rooms in entire grid

public:
	//Default constructor
	Map(int width, int height, unsigned int max);
	//Telling compiler I don't want a copy constructor
	//Map(Map & map const) = delete;

	//Function to check for adj rooms
	bool checkRooms(int posX, int posY, int moveX, int moveY);

	//Destructor
	~Map();

	//FUnction to set level start, boss and enemy rooms
	void setRooms();
	//Function to set all room connectors
	void room_Connectors();

	//Function to removeExcessRooms
	//void remExcessRooms();

	//Function to set current room
	void setCurrentRoom(Room* newCurrent);
	//Function to get current room
	Room* getCurrentRoom();
	
	//Enemy Generator for basic enemy rooms
	void CreateEnemy();

	//Function to move player into center of Boss Room when entering
	//Bottom left oriented

	


};



