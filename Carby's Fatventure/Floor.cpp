#include "Floor.h"

floortiles::floortiles()
{
	positionX = -248.0f;
	positionY = -248.0f;

	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);
	floorTexture = AEGfxTextureLoad("..\\Assets\\TestFloor.png");

	tileMesh = AEGfxMeshEnd();
}

floortiles::floortiles(float posX, float posY)
{
	positionX = posX;
	positionY = posY;

	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);
	floorTexture = AEGfxTextureLoad("..\\Assets\\TestFloor.png");

	tileMesh = AEGfxMeshEnd();
}

void floortiles::drawfloorGraphics(int mapsizeX, int mapsizeY)
{
	float resetY = positionY;
	float resetX = positionX;

	for (int counterY = 0; counterY < mapsizeY; ++counterY)
	{
		positionX = resetX;
		for (int counterX = 0; counterX < mapsizeX; ++counterX)
		{
			AEGfxSetBlendMode(AE_GFX_BM_BLEND);
			AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
			AEGfxSetPosition(positionX, positionY);
			AEGfxTextureSet(floorTexture, 0, 0);
			AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
			AEGfxSetTransparency(1.0f);
			AEGfxMeshDraw(tileMesh, AE_GFX_MDM_TRIANGLES);

			positionX += 64.0f;
		}
		positionY += 64.0f;
	}

	positionX = resetX;
	positionY = resetY;
}

floortiles::~floortiles()
{
	AEGfxTextureUnload(floorTexture);

	AEGfxMeshFree(tileMesh);
}