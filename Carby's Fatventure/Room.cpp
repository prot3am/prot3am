#include "Room.h"

Room::Room()
{
	// Note: replace accordingly when read-in binary map is available
	AEMtx33 scale, rot, trans;

	// Compute the scaling matrix always 1 unit tile
	AEMtx33Scale(&scale, 1.0f, 1.0f);

	MapSizeX = 13;
	MapSizeY =  9;
	for (int i = 0; i < MapSizeX; i++)
	{
		for (int j = 0; j < MapSizeY; j++)
		{
			Node newNode = Node(i, j);
			newNode.setPositionCoord(i * 64.0f - 384, j * 64.0f - 256);
			////////////////////////////////////////////////////////////
			// Set different tile types for different nodes
			////////////////////////////////////////////////////////////
			if (i == 0 || j == 0 || i == MapSizeX - 1 || j == MapSizeY - 1)
			{
				newNode.SetNODETYPE(NONWALKABLE);
				newNode.SetTileType(WALL);
			}

			///////////////////////////////////////////////////////////
			// Set the transformation matrix for each tile/node
			
			if (i == 0 && j > 0 && j != MapSizeY - 1) // walls on left side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 1.57f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i == MapSizeX - 1 && j > 0 && j != MapSizeY - 1) // walls on right side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 4.71f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i > 0 && j == MapSizeY - 1 && i != MapSizeX - 1) // walls on top side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 0.0f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i > 0 && j == 0 && i != MapSizeX - 1) //walls on btm side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 3.14f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i == 0 && j == 0) //For bottom left corner
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 1.57f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}
			
			else if (i == 0 && j == MapSizeY - 1) //For top left corner
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 0.0f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i == MapSizeX - 1 && j == MapSizeY - 1) //For top right corner
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 4.71f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i == MapSizeX - 1 && j == 0) //For bottom right corner
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 3.14f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else //For all floor tiles
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &rot);
			}

			//////////////// END OF TILE TYPE SET //////////////////////
			GridNodes[i + j * MapSizeX] = newNode;
		}
	}

	startEnemy = nullptr;
	boss = nullptr;

	InitRender();

	/*floor = new floortiles(-256, -256);
	wallTop = new walltiles[16];
	wallBtm = new walltiles[16];
	wallLeft = new walltiles[14];
	wallRight = new walltiles[14];
	walltest = new walltiles*[4];

	walltest[0] = new walltiles[16];
	walltest[1] = new walltiles[16];
	walltest[2] = new walltiles[14];
	walltest[3] = new walltiles[14];

	float x1 = -312.0f;
	float x2 = 256.0f;
	float y1 = -312.0f;
	float y2 = 256.0f;

	for (int i = 0; i < 16; ++i)
	{
		walltest[0][i].setPositionCoord(x1, y1);
		walltest[1][i].setPositionCoord(x1, y2);
		x1 += 64.0f;
	}
	x1 = -312.0f;
	for (int i = 0; i < 14; ++i)
	{
		walltest[2][i].setPositionCoord(x1, y1);
		walltest[3][i].setPositionCoord(x2, y1);
		y1 += 64.0f;
	}*/
}

Room::Room(int x, int y)
{
	// Note: replace accordingly when read-in binary map is available
	AEMtx33 scale, rot, trans;
	// Compute the scaling matrix always 1 unit tile
	AEMtx33Scale(&scale, 1.0f, 1.0f);

	MapSizeX = x;
	MapSizeY = y;
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			Node newNode = Node(i, j);
			newNode.setPositionCoord(i * 64.0f - 384, j * 64.0f - 296);

			////////////////////////////////////////////////////////////
			// Set different tile types for different nodes
			////////////////////////////////////////////////////////////
			if (i == 0 || j == 0 || i == x - 1 || j == y - 1)
			{
				newNode.SetNODETYPE(NONWALKABLE);
				newNode.SetTileType(WALL);
			}

			///////////////////////////////////////////////////////////
			// Set the transformation matrix for each tile/node

			if (i == 0 && j > 0) // walls on left side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 1.57f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i > 0 && j == y - 1) // walls on bottom side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 3.14f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			else if (i == x - 1 && j != y - 1) // walls on right side
			{
				AEVec2 nodePos = newNode.getcurrPos();

				AEMtx33Trans(&trans, nodePos.x, nodePos.y);

				AEMtx33Rot(&rot, 4.71f);

				AEMtx33Concat(&newNode.GetTransformMtx(), &rot, &scale);
				AEMtx33Concat(&newNode.GetTransformMtx(), &trans, &newNode.GetTransformMtx());
			}

			//////////////// END OF TILE TYPE SET //////////////////////
			GridNodes[i + (j * x)] = newNode;
		}
	}
	
	startEnemy = nullptr;
	boss = nullptr;
	
	InitRender();

	/*floor = new floortiles(-256, -256);
	walltest = new walltiles*[4];

	walltest[0] = new walltiles[x];
	walltest[1] = new walltiles[x];
	walltest[2] = new walltiles[y - 2];
	walltest[3] = new walltiles[y - 2];

	float x1 = -320.0f;
	float x2 = 256.0f;
	float y1 = -320.0f;
	float y2 = 256.0f;

	for (int i = 0; i < x; ++i)
	{
		walltest[0][i].setPositionCoord(x1, y1);
		walltest[1][i].setPositionCoord(x1, y2);
		x1 += 64.0f;
	}
	x1 = -256.0f;
	for (int i = 0; i < y - 2; ++i)
	{
		walltest[2][i].setPositionCoord(x1, y1);
		walltest[3][i].setPositionCoord(x2, y1);
		y1 += 64.0f;
	}*/
}

Room::~Room()
{
	for (int i = 0; i < MapSizeX; i++)
	{
		for (int j = 0; j < MapSizeY; j++)
		{
			delete[] & (GridNodes[i + j * MapSizeX]);
		}
	}

	while (!isEmpty())
	{
		RemoveEnemy(0);
	}

	AEGfxTextureUnload(cornerwallTexture);
	AEGfxTextureUnload(midwallTexture);
	AEGfxTextureUnload(leftsidewallTexture);
	AEGfxTextureUnload(rightsidewallTexture);
	AEGfxTextureUnload(floorTexture);
	AEGfxMeshFree(nodeMesh);
}

void Room::InitRender()
{
	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);

	nodeMesh = AEGfxMeshEnd();

	cornerwallTexture = AEGfxTextureLoad("..\\Assets\\TestWallCorner.png");
	midwallTexture = AEGfxTextureLoad("..\\Assets\\TestWallMid.png");
	leftsidewallTexture = AEGfxTextureLoad("..\\Assets\\TestWallRight.png");
	rightsidewallTexture = AEGfxTextureLoad("..\\Assets\\TestWallLeft.png");
	floorTexture = AEGfxTextureLoad("..\\Assets\\TestFloor.png");
}

void Room::Update(float deltaTime, Entity *target)
{
	//Update for Room (If any)

	//Update for Enemy
	Enemy *itPtr = startEnemy;
	for (int i = 0; i < enemyCount; ++i)
	{
		if (itPtr->getHP() == 0)
		{
			RemoveEnemy(i);
			--i;//Decrement one so the system will detect the next one
		}
		else
		{
			itPtr->Update(deltaTime, target);
		}

		itPtr = itPtr->Next();
	}

	//Update for Boss

}

void Room::Draw()
{
	//Draws Map
	for (int i = 0; i < MapSizeX; ++i)
	{
		for (int j = 0; j < MapSizeY; ++j)
		{
			Node *toDraw = getNode(i,j);
			AEGfxSetBlendMode(AE_GFX_BM_BLEND);
			AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
		
				
			if (getNode(i, j)->GetWalkable() == WALKABLE && getNode(i, j)->GetTileType() == FLOOR)
				AEGfxTextureSet(floorTexture, 0, 0);
			else if (i == 0 && (j == 0 || j == MapSizeY - 1) || i == MapSizeX - 1 && (j == 0 || j == MapSizeY - 1))
				AEGfxTextureSet(cornerwallTexture, 0, 0);
			else if ((i == 0 && j == MapSizeY / 2 )||( i == MapSizeX / 2 && j == 0 )||( i == MapSizeX - 1 && j == MapSizeY / 2) || (i == MapSizeX / 2 && j == MapSizeY - 1))
				AEGfxTextureSet(midwallTexture, 0, 0);
			else if(i == 0 && j > MapSizeY/2 || i < MapSizeX/2 && j == 0 || i == MapSizeX - 1 && j < MapSizeY/2 || i > MapSizeX/2 && j == MapSizeY - 1) // left side wall texture
				AEGfxTextureSet(leftsidewallTexture, 0, 0);
			else if(i == 0 && j < MapSizeY/2 || i > MapSizeX/2 && j == 0 || i == MapSizeX - 1 && j > MapSizeY/2 || i < MapSizeX/2 && j == MapSizeY - 1) // right side wall texture
				AEGfxTextureSet(rightsidewallTexture, 0, 0);
			
			AEGfxSetTransform(toDraw->GetTransformMtx().m);
			AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
			AEGfxSetTransparency(1.0f);
			AEGfxMeshDraw(nodeMesh, AE_GFX_MDM_TRIANGLES);
			//TODO: Draw fire on tile if its fire
		}
	}

	//Draws for Enemy
	Enemy *itPtr = startEnemy;
	for (int i = 0; i < enemyCount; ++i)
	{
		itPtr->Draw();
		itPtr = itPtr->Next();
	}
}

//Pathfinder
bool Room::checkSurroundingNodes(Node *centerN, DIRECTION dir)
{
	bool available = false;
	if (dir == TOP)
	{
		if (centerN->GetYCord() > 0)
			available = true;
	}
	else if (dir == TOPRIGHT)
	{
		if (centerN->GetXCord() < MapSizeX - 1 &&
			centerN->GetYCord() > 0)
			available = true;
	}
	else if (dir == RIGHT)
	{
		if (centerN->GetXCord() < MapSizeX - 1)
			available = true;
	}
	else if (dir == BTMRIGHT)
	{
		if (centerN->GetXCord() < MapSizeX - 1 &&
			centerN->GetYCord() < MapSizeY - 1)
			available = true;
	}
	else if (dir == BTM)
	{
		if (centerN->GetYCord() < MapSizeY - 1)
		{
			available = true;
		}
	}
	else if (dir == BTMLEFT)
	{
		if (centerN->GetXCord() > 0 &&
			centerN->GetYCord() < MapSizeY - 1)
		{
			available = true;
		}
	}
	else if (dir == LEFT)
	{
		if (centerN->GetXCord() > 0)
			available = true;
	}
	else if (dir == TOPLEFT)
	{
		if (centerN->GetXCord() > 0 &&
			centerN->GetYCord() > 0)
			available = true;
	}
	return available;
}

Node *Room::getSurroundingNode(Node *centerN, DIRECTION dir)
{
	Node *toReturn = nullptr;
	int centerX, centerY;

	centerX = centerN->GetXCord();
	centerY = centerN->GetYCord();

	switch (dir)
	{
	case TOP:
	{
		toReturn = (GridNodes + centerX) + (centerY - 1) * MapSizeX;
		break;
	}
	case TOPRIGHT:
	{
		toReturn = (GridNodes + centerX + 1) + (centerY - 1) * MapSizeX;
		break;
	}
	case RIGHT:
	{
		toReturn = (GridNodes + centerX + 1) + centerY * MapSizeX;
		break;
	}
	case BTMRIGHT:
	{
		toReturn = (GridNodes + centerX + 1) + (centerY + 1) * MapSizeX;
		break;
	}
	case BTM:
	{
		toReturn = (GridNodes + centerX) + (centerY + 1) * MapSizeX;
		break;
	}
	case BTMLEFT:
	{
		toReturn = (GridNodes + centerX - 1) + (centerY + 1) * MapSizeX;
		break;
	}
	case LEFT:
	{
		toReturn = (GridNodes + centerX - 1) + centerY * MapSizeX;
		break;
	}
	case TOPLEFT:
	{
		toReturn = (GridNodes + centerX - 1) + (centerY - 1) * MapSizeX;
		break;
	}

	}

	return toReturn;

}

Node *Room::findPath(Node *startN, Node *endN)
{
	Node * currentN = startN;
	Node destN = *endN;
	int openListCount = 1;
	resetPNodes();

	currentN->MoveToOpenList();
	currentN->SetAStarValues(0.0f, (float)((destN.GetXCord() - currentN->GetXCord()) * (destN.GetXCord() - currentN->GetXCord())
		+ (destN.GetYCord() - currentN->GetYCord()) * (destN.GetYCord() - currentN->GetYCord())));

	//Either one of two conditions must be met to get out of Pathfinding
	//- Reached the end node (Found shortest path)
	//- Unable to find end node (All paths found)
	while (openListCount)
	{
		float finalF = 1000.0f;
		//For the X Array
		for (int i = 0; i < MapSizeX /*sizeof(GridNodes) / sizeof(GridNodes[0])*/; i++)
		{
			//For the Y Array
			for (int j = 0; j < MapSizeY /*sizeof(GridNodes[0]) / sizeof(GridNodes[0][0])*/; j++)
			{
				//If it is on the open list

				if (GridNodes[i + j * MapSizeX].GetCurrentList() == 1)
				{
					if (GridNodes[i + j * MapSizeX].GetFVal() < finalF)
					{
						currentN = &GridNodes[i + j * MapSizeX];
						finalF = currentN->GetFVal();
					}
				}
			}
		}

		if (currentN->GetXCord() == destN.GetXCord() && currentN->GetYCord() == destN.GetYCord())
		{
			break;
		}

		//Smallest F value (Optimal node is found)
		currentN->MoveToClosedList();
		openListCount--;

		for (int i = 1; i < 9; i++)
		{
			DIRECTION dir = (DIRECTION)i;
			if (checkSurroundingNodes(currentN, dir))
			{
				Node * tempNode = getSurroundingNode(currentN, dir);

				//Ignore if in closed list, double check if in open list, add if not in either lists
				if (tempNode->GetCurrentList() == 2)
				{
				}
				else if (tempNode->GetCurrentList() == 1)
				{
					float f, g, h;

					//G value should not change, but if is diagonal, set 1.4 instead
					if (i % 2)
						g = currentN->GetGVal() + 1;
					else
						g = currentN->GetGVal() + 1.4f;
					h = (*tempNode).GetHVal();
					f = g + h;

					if (f < (*tempNode).GetFVal())
					{
						(*tempNode).SetAStarValues(g, h);
						(*tempNode).SetParentNode(currentN->GetXCord(), currentN->GetYCord());
					}
				}
				else if ((*tempNode).GetCurrentList() == 0 && (*tempNode).GetWalkable())
				{
					float g, h; //f

					//If it is odd (1,3,5,7), it is a cardinal direction, otherwise it is a diagonal
					if (i % 2)
						g = currentN->GetGVal() + 1;
					else
						g = currentN->GetGVal() + 1.4f;
					h = (float)(((*tempNode).GetXCord() - endN->GetXCord()) * ((*tempNode).GetXCord() - endN->GetXCord()) +
						((*tempNode).GetYCord() - endN->GetYCord()) * ((*tempNode).GetYCord() - endN->GetYCord()));
					tempNode->SetAStarValues(g, h);
					tempNode->SetParentNode(currentN->GetXCord(), currentN->GetYCord());
					tempNode->MoveToOpenList();
					openListCount++;
				}
			}
		}//End Adding/Assigning surrounding nodes

	}//End Pathfinding while loop

	return currentN;
}

void Room::resetPNodes()
{
	for (int i = 0; i < MapSizeX; ++i)
	{
		for (int j = 0; j < MapSizeY; ++j)
		{
			GridNodes[i + j * MapSizeX].SetParentNode(-1, -1);
		}
	}
}

//Enemy Detection
bool Room::inRange(Node *startN, Node *targetN, int range)
{
	int xDist = startN->GetXCord() - targetN->GetXCord();
	int yDist = startN->GetYCord() - targetN->GetYCord();

	if (xDist * xDist + yDist * yDist < range * range)
		return true;

	return false;
}

//Usually the width would be odd
bool Room::inRange(Node *startN, Node *targetN, int range, int width, DIRECTION dir)
{
	bool isInRange = false;
	//If Cardinal directions
	if (dir % 2)
	{
		int xMin = startN->GetXCord();
		int yMin = startN->GetYCord();
		//Temp initialize Max values to be 0
		int xMax = 0;
		int yMax = 0;
		
		switch (dir)
		{
		case TOP:
			xMin -= (width - 1) / 2;
			yMin -= (range + 1);
			xMax = xMin + width;
			yMax = yMin + range;
			break;
		case RIGHT:
			xMin += 1;
			yMin -= (width - 1) / 2 - 1;
			xMax = xMin + range;
			yMax = yMin + width;
			break;
		case BTM:
			xMin -= (width - 1) / 2;
			yMin += range + 1;
			xMax = xMin + width;
			yMax = yMin + range;
			break;
		case LEFT:
			xMin -= range + 1;
			yMin -= (width - 1) / 2;
			xMax = xMin + range;
			yMax = yMin + width;
			break;
		default:
			break;
		}

		//Goes through newly formed square to check each node
		for (int i = xMin; i < xMax; ++i)
		{
			for (int j = yMin; j < yMax; ++j)
			{
				//Checks for validity
				//No need to check out of bounds
				if (i == targetN->GetXCord() && j == targetN->GetYCord())
				{
					isInRange = true;
					break;
				}
				else
				{
					continue;
				}
			}
			if (isInRange)
				break;
		}
	}
	else //if it is diagonal
	{
		//vec1 acts as movement direction, vec2 act as width direction
		AEVec2 vec1 = { 0, 0 };
		AEVec2 vec2 = { 0, 0 };
		AEVec2 min;

		min.x = (f32)startN->GetXCord();
		min.y = (f32)startN->GetYCord();

		switch (dir)
		{
		case TOPRIGHT:
			vec1 = { 1,-1 };
			vec2 = { 1, 1 };
			min.x -= vec2.x * (width - 1) / 2;
			min.y -= vec2.y * (width - 1) / 2;
			break;
		case BTMRIGHT:
			vec1 = { 1, 1 };
			vec2 = {-1, 1 };
			min.x -= vec2.x * (width - 1) / 2;
			min.y -= vec2.y * (width - 1) / 2;
			break;
		case BTMLEFT:
			vec1 = {-1, 1 };
			vec2 = {-1, -1};
			min.x -= vec2.x * (width - 1) / 2;
			min.y -= vec2.y * (width - 1) / 2;
			break;
		case TOPLEFT:
			vec1 = {-1,-1 };
			vec2 = {-1, 1 };
			min.x -= vec2.x * (width - 1) / 2;
			min.y -= vec2.y * (width - 1) / 2;
			break;
		default:
			break;
		}

		for (int i = 0; i < width; ++i)
		{
			for (int j = 0; j < range; ++j)
			{
				//Checks for validity
				//No need to check out of bounds
				if (min.x + i * vec2.x + j * vec1.x == targetN->GetXCord() && 
					min.y + i * vec2.y + j * vec2.y == targetN->GetYCord())
				{
					isInRange = true;
					break;
				}
				else
				{
					continue;
				}
			}
			if (isInRange)
				break;
		}
	}
	return isInRange;
}

bool Room::hasLineOfSight(Node *startN, Node *targetN)
{
	bool hasLOS = true;
	if(startN->GetXCord() != targetN->GetXCord() && startN->GetYCord() != targetN->GetYCord())
	{ 
		//Total Distance between X coordinates and Y Coordinates respectively
		int deltaX = targetN->GetXCord() - startN->GetXCord();
		int deltaY = targetN->GetYCord() - startN->GetYCord();
		int deltaErr = deltaY/ deltaX;
		float deviation = 0.0f;
		
		//Sets delta to absolute
		if (deltaErr < 0)
			deltaErr *= -1;

		int curX = startN->GetXCord();
		int curY = startN->GetYCord();

		//If start node is less than target node
		if(deltaX > 0)
			for (; curX != targetN->GetXCord(); ++curX)
			{
				if (!GridNodes[curX + curY * MapSizeX].GetWalkable())
				{
					hasLOS = false;
					break;
				}
				deviation += deltaErr;

				if (deviation >= 0.5f)
				{
					if (deltaY > 0)
						++curY;
					else
						--curY;
					deviation -= 1.0f;
				}
			}
		else
			for (; curX != targetN->GetXCord(); --curX)
			{
				if (!GridNodes[curX + curY * MapSizeX].GetWalkable())
				{
					hasLOS = false;
					break;
				}
				deviation += deltaErr;

				if (deviation >= 0.5f)
				{
					if (deltaY > 0)
						++curY;
					else
						--curY;
					deviation -= 1.0f;
				}
			}
	}
	else if (startN->GetXCord() == targetN->GetXCord())
	{
		int curY = startN->GetYCord();
		if (curY < targetN->GetYCord())
			for (; curY != targetN->GetYCord(); ++curY)
			{
				if (!(GridNodes[curY * MapSizeX + startN->GetXCord()].GetWalkable()))
				{
					hasLOS = false;
					break;
				}
				else
					continue;
			}
		else
			for (; curY != targetN->GetYCord(); --curY)
			{
				if (!(GridNodes[curY * MapSizeX + startN->GetXCord()].GetWalkable()))
				{
					hasLOS = false;
					break;
				}
				else
					continue;
			}
	}
	else if (startN->GetYCord() == targetN->GetYCord())
	{
		int curX = startN->GetXCord();
		if (curX < targetN->GetXCord())
			for (; curX != targetN->GetXCord(); ++curX)
			{
				if (!(GridNodes[curX + startN->GetYCord() * MapSizeX].GetWalkable()))
				{
					hasLOS = false;
					break;
				}
				else
					continue;
			}
		else
			for (; curX != targetN->GetXCord(); --curX)
			{
				if (!(GridNodes[curX + startN->GetYCord() * MapSizeX].GetWalkable()))
				{
					hasLOS = false;
					break;
				}
				else
					continue;
			}
	}
	return hasLOS;
}

//Getter/Setter Functions

int Room::mapX()
{
	return MapSizeX;
}

int Room::mapY()
{
	return MapSizeY;

}

Node* Room::getNode(int x, int y)
{
	return &(GridNodes[x + y * MapSizeX]);
}

void Room::setDoors(int pos)
{
	doors[pos] = true;
}

bool Room::getDoors(int pos)
{
	return (doors[pos]);
}

void Room::setIdentity (roomID ID)
{
	identity = ID;
}

roomID Room::getIdentity()
{
	return identity;
}

//For Enemy List
bool Room::isEmpty()
{
	if (startEnemy)
		return false;
	else
		return true;
}

void Room::AddEnemy(Enemy *newEnemy)
{
	//Constructs and assigns enemy
	Enemy *theEnemy = new Enemy(*newEnemy);
	theEnemy->setRoom(this);
	theEnemy->setPosition(&GridNodes[0]); //Change the position elsewhere

	if (startEnemy)
	{
		Enemy *itPtr = startEnemy;
		while (itPtr->Next())
		{
			itPtr = itPtr->Next();
		}
		itPtr->setNext(theEnemy);
	}
	else //No enemy found
	{
		startEnemy = theEnemy;
		//Move this to template gen in future
		startEnemy->InitRender();
	}
	++enemyCount;
}

void Room::RemoveEnemy(int pos)
{
	//If it exists
	if (enemyCount > pos)
	{
		if (pos)
		{
			Enemy *itPtr = startEnemy;
			Enemy *prevItPtr = nullptr;

			//Iterates through List until it finds the right spot
			for (int i = 0; i < pos; ++i)
			{
				prevItPtr = itPtr;
				itPtr = itPtr->Next();
			}

			prevItPtr->setNext(itPtr->Next());
			delete itPtr;
			--enemyCount;
		}
		else
		{
			Enemy *toErase = startEnemy;
			startEnemy = startEnemy->Next();
			delete toErase;
			--enemyCount;
		}
	}
}

void Room::SetEnemyActive(bool activeState)
{
	Enemy* itPtr = startEnemy;
	for (int i = 0; i < enemyCount; ++i)
	{
		itPtr->setActive(activeState);
		itPtr = itPtr->Next();
	}
}

/*void Room::EnemyUpdate(float deltaTime, Enemy *actor, Entity *target)
{
	if (actor->active())
	{
		if (actor->getHP() < 1)
		{
			actor->setActive(false);
		}
		else
		{
			float curAtkCD = actor->getCurAtkCD();
			float atkCD = actor->getAtkCD();

			switch (actor->getBehavior())
			{
			case MELEE:
				if (inRange(actor->getNode(), target->getNode(), actor->getRange()))
				{
					if (curAtkCD < 0.0f)
					{
						//Attack Target
						actor->setCurAtkCD(atkCD);
						target->takeDamage(actor->getAtk());
					}
				}
				else
				{
					//Run to player
				}
				curAtkCD -= deltaTime;
				break;
			case MELEEAOE:
				if (inRange(actor->getNode(), target->getNode(), actor->getRange(), actor->getWidth(), actor->getDir()))
				{
					if (curAtkCD < 0.0f)
					{
						//Attack Target
						actor->setCurAtkCD(atkCD);
						target->takeDamage(actor->getAtk());
					}
				}
				else
				{
					//Run to player
				}
				curAtkCD -= deltaTime;
				break;
			case RANGE:
				if (hasLineOfSight(actor->getNode(), target->getNode()))
				{
					if (curAtkCD < 0.0f)
					{
						//Attack Target
						actor->setCurAtkCD(atkCD);
						target->takeDamage(actor->getAtk()); //This will be changed to spawning bullets
					}
				}
				curAtkCD -= deltaTime;
				break;
			default:
				break;
			}
		}
	}
}*/

//void Room::BossUpdate(float deltaTime, Entity *target)
//{
	//To Implement
	//Pseudo Code:
	/*	Boss has up to five function pointers to determine sequence of attacks.
		Boss initializes first five function pointers to point to different abilities in the library
		Boss calls them in order FP1 -> FP2 -> FP3 -> FP4 -> FP5, which access the ability in the library
		If a Function Pointer is empty, skip to next one. After FP5, revert back to FP1
		Possibly shift Boss Update to Map instead, and have library at that level as well
	*/
//}
