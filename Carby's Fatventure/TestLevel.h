#pragma once
#include "AEEngine.h"
#include "Player.h"

void TestLevel_Load();

void TestLevel_Init();

void TestLevel_Update(double dt);

void TestLevel_Draw();

void TestLevel_Free();

void TestLevel_Unload();
