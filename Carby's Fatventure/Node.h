#pragma once
#include "CollisionBox.h"
#include "NODETYPE.h"
#include "TileTypeList.h"
#include "AEEngine.h"

class Node :
	public hitBox
{
public:
	Node();
	Node(int x, int y);
	~Node();
	void SetCoordinates(int x, int y);
	void SetParentNode(int x, int y);
	void SetAStarValues(float g, float h);
	void SetNODETYPE(NODETYPE walkable);
	void SetTileType(TILETYPE tiletype);

	void MoveToOpenList();
	void MoveToClosedList();
	void MoveOutOfList();

	NODETYPE GetWalkable();
	int GetXCord();
	int GetYCord();
	float GetFVal();
	float GetGVal();
	float GetHVal();
	int GetCurrentList();
	int GetParentX();
	int GetParentY();
	AEMtx33& GetTransformMtx();
	TILETYPE GetTileType() const;


private:
	int xCord, yCord; //Self node coordinates
	int pXCord, pYCord; //Parent Node coordinates
	bool inOpenList, inClosedList; //For A* Pathfinding
	NODETYPE walkable;
	float f, g, h; //g represents distance from start, h represents distance left, f represents total cost (g+h)

	TILETYPE tileType; //tiletype (floor,wall,open door or closed door)
	AEMtx33 transformMtx;
};

