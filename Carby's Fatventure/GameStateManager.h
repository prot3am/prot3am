/* Start Header ************************************************************************/
/*!
\file GameStateManager.h
\author Teh Yu Yin, t.yuyin, 390005118
\par t.yuyin\@digipen.edu
\date 02/02/2019
\brief
This file contains the function declarations for initializing and updating the game state manager.

Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents
without the prior written consent of DigiPen Institute of
Technology is prohibited.
*/
/* End Header **************************************************************************/
#include "GameStateList.h"
#pragma once

typedef void(*FPU)(double dt);

typedef void(*FP)(void);

extern int _current, _previous, _next;                         // current game state status

extern FP fpLoad, fpInit, fpDraw, fpFree, fpUnload;  // function pointers

extern FPU fpUpdate;
// ----------------------------------------------------------------------------
// This function sets up the game state manager
// It should be called once after system initialization
// It starts up a game state upon initialization
// ----------------------------------------------------------------------------
void GSM_Initialize(int startingState);

// ----------------------------------------------------------------------------
// This function updates the game states of the game state manager
// It should be called for as long as the game runs
// It checks and updates function pointers accordingly
// ----------------------------------------------------------------------------
void GSM_Update();