#include "Boss.h"

Boss::Boss()
{
}

void Boss::ChangeState(BossState NewState)
{
	CurrentState = NewState;
}

void Boss::RevertState()
{
	CurrentState = InitState;
}

void Boss::SetInitState(BossState State)
{
	InitState = State;
}

Boss::~Boss()
{
}
