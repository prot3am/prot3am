#include "Entity.h"

Entity::Entity()
{
	setStats(1, 1, 1);
}

Entity::Entity(int HP, int Atk, float Spd)
{
	setStats(HP, Atk, Spd);
}

Entity::~Entity()
{
}

void Entity::setStats(int newHP, int newAtk, float newSpd)
{
	hp = newHP;
	atk = newAtk;
	spd = newSpd;
}

void Entity::setName(string newName)
{
	name = newName;
}

void Entity::setHP(int newHP)
{
	hp = newHP;
}

void Entity::setAtk(int newAtk)
{
	atk = newAtk;
}

void Entity::setDef(int newDef)
{
	def = newDef;
}

void Entity::setSpd(float newSpd)
{
	spd = newSpd;
}

void Entity::setActive(bool Active)
{
	isActive = Active;
}

/*void Entity::setPosition(int x, int y)
{
	//TODO: Uncomment when map is over
	currentNode = currentRoom.getNode(x,y);
}*/

void Entity::setPosition(Node *newNode)
{
	currentNode = newNode;
}

void Entity::setDirection(DIRECTION newDir)
{
	direction = newDir;
}

void Entity::setSize(int x, int y)
{
	sizeX = x;
	sizeY = y;
}

string Entity::Name()
{
	return name;
}

int Entity::getHP()
{
	return hp;
}

int Entity::getAtk()
{
	return atk;
}

int Entity::getDef()
{
	return def;
}

float Entity::getSpd()
{
	return spd;
}

bool Entity::active()
{
	return isActive;
}

Node* Entity::getNode()
{
	return currentNode;
}

int Entity::getXPos()
{
	return currentNode->GetXCord();
}

int Entity::getYPos()
{
	return currentNode->GetYCord();
}

DIRECTION Entity::getDir()
{
	return direction;
}

int Entity::getWidth()
{
	return sizeX;
}

int Entity::getHeight()
{
	return sizeY;
}

bool Entity::takeDamage(int damage)
{
	while (damage > 0)
	{
		if (def > hp)
			--def;
		else
			--hp;
		--damage;
	}

	if (hp < 0)
		return 1;
	else
		return 0;
}

/*void Entity::Update(float deltaTime)
{
	if (isActive)
	{

	}
}*/