#pragma once

#include "AEEngine.h"

class hitBox
{
public:
	hitBox();
	~hitBox();
	bool chkAABB(hitBox &obj2);
	void setLengths(float lengthX, float lengthY);
	void setScale(float setscale);
	void setVelocity(float velX, float velY);
	void setPositionCoord(float x, float y);
	void minmaxUpdate();
	AEVec2 getcurrPos();

protected:
	float scale;    // default at 1.0f per unit square, adjusted for Boss hitbox
	AEVec2 lengths; // half width & height of hitbox
	AEVec2 min;
	AEVec2 max;

	float velocityX;
	float velocityY;

	float positionX;
	float positionY;
};