#pragma once
#include <AEVec2.h>

class ParticleEffect
{
public:
	ParticleEffect();
	ParticleEffect(AEVec2 dir, float accel, float spd, float dur, AEVec2 pos);
	~ParticleEffect();

	bool isActive();
	bool checkActive();
	void deactivate();
	void SetParams(AEVec2 dir, float accel, float spd, float dur, AEVec2 pos);
	void Update(float dt);

private:
	float particleSpd; //Determines velocity of particles
	float particleAccel; //Determines speed of particles
	float particleDur; //Determines the duration of particle
	float particleTimer; //Timer to check for duratiion for particle timer
	AEVec2 particleDir; //Normal vector of the direction to move in
	AEVec2 particlePos; //Determines position of particle
	bool active; //Flag to determine if the particle is active
};

