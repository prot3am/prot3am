#pragma once
#include <map>
#include <iostream>
#include <fstream>

using std::string;

class Dictionary
{
public:
	Dictionary();
	~Dictionary();

	std::map<string, int> atkMap;
	std::map<string, int> ablMap;
	std::map<string, int> sklMap;

	void ReadToMap(int mapType);
	void WriteToMap(int mapType);

	string GetAttack(int key);
	void DisplayAttacks();

	string GetSkill(int key);
	void DisplaySkills();

	string GetAbility(int key);
	void DisplayAbility();

};

