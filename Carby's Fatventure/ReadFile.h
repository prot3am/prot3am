#pragma once

#include <iostream>
#include <string>
#include <fstream>

//using std::string;
using namespace std;

class ReadFileEnemy
{
public:
	ReadFileEnemy(string Name, int HP, int Atk, int Spd, int behaviour, int Direction, 
					int Level, int Range, int Width, int AtkCD);

	//get name data from file
	void setName(string name);
	string getName();
	
	//get HP data from file
	void setHP(int hp);
	int getHP();
	
	//get Atk data from file
	void setAtk(int atk);
	int getAtk();

	//get Spd data from file
	void setSpd(int x);
	int getSpd();

	//get behaviour data from file
	void setBehaviour(int behaviour);
	int getBehaviour();

	//get Direction data from file
	void setDirection(int direction);
	int getDirection();

	//get Level data from file
	void setLevel(int level);
	int getLevel();

	//get Range data from file
	void setRange(int range);
	int getRange();

	//get Width data from file
	void setWidth(int width);
	int getWidth();

	//get AtkCD data from file
	void setAtkCD(int atkCD);
	int getAtkCD();

	void ReadInEnemyData();
private:
	string EnemyName;
	int EnemyHP;
	int EnemyAtk;
	int EnemySpd;
	int EnemyBehaviour;
	int EnemyDirection;
	int EnemyLevel;
	int EnemyRange;
	int EnemyWidth;
	int EnemyAtkCD;

};

class Milestone
{
	Milestone();
public:
	int getEnemyKilled();
	int getDamageDone();
	int getDamageRecieved();
	int getGoldRecieved();
	int getUpgradesDone();
	int getEnergyCost();

private:
	int EnemyKilled;
	int DamageDone;
	int DamageRecieved;
	int GoldRecieved;
	int UpgradesDone;
	int EnergyCost;
};
void loadsMilstoneAchievement();
void testing_getName(ReadFileEnemy &x);