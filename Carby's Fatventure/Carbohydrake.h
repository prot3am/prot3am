#pragma once
#include "Boss.h"
#include "Slot.h"
class Carbohydrake :
	public Boss
{
public:
	Carbohydrake();
	~Carbohydrake();

	void Update(float deltaTime);

	//An attack that hits multiple different tiles in the Room and sets them on fire
	void MeatierShower();
	//A flamethrower attack with long setup/delay
	void ToastyTendies();
	//A faster attack by swiping at the player
	void CarbonaraCrash();
	//A movement ability that pushes the player away
	void SeasoningGreetings();
};

