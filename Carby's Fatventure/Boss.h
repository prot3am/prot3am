#pragma once
#include "Entity.h"
#include "Slot.h"
#include <random>
#include "AIBehavior.h"
class Boss :
	public Entity
{
public:

	Boss();
	~Boss();

	//Changes the behavior of boss
	void ChangeState(BossState NewState);
	//Reverts the behavior of boss to inital state
	void RevertState();
	//Sets the initial state of boss
	void SetInitState(BossState state);


protected:
	Slot *skillSlot[4]; //This records the skills that would drop/be used
	BossState CurrentState; /*This dictates the boss' next attack*/
	BossState InitState; /*This sets the inital stage of the boss*/
	int AtkCount;
	int counter; //Used for misc purposes

	//Delay before attack
	float curAtkDelay;
	//Duration of current attack left
	float curAtkDur;
	//Cooldown until next attack 
	float curAtkCD;
};

