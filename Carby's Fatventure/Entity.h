#pragma once
#include <string>
#include "Node.h"
#include "Direction.h"
#include "CollisionBox.h"
using std::string;

class Entity :
	public hitBox
{
public:
	Entity();
	Entity(int HP, int Atk, float Spd);
	~Entity();
	
	virtual void setStats(int HP, int Atk, float Spd);
	void setName(string name);
	void setHP(int HP);
	void setAtk(int Atk);
	void setDef(int Def);
	void setSpd(float Spd);
	void setActive(bool Active);
	//void setPosition(int x, int y);
	void setPosition(Node *newNode);
	void setDirection(DIRECTION dir);
	void setSize(int x, int y);

	string Name();
	int getHP();
	int getAtk();
	int getDef();
	float getSpd();
	Node* getNode();
	int getXPos();
	int getYPos();
	bool active();
	DIRECTION getDir();
	int getWidth();
	int getHeight();

	//Reduces the entity's health/defence. Returns true if hp is still above 0
	bool takeDamage(int damage);
	//virtual void Update(float deltaTime);

protected:
	string name;
	Node *currentNode;
	DIRECTION direction;
	int hp;
	int maxHP;
	int atk;
	int def;
	float spd;
	int sizeX;
	int sizeY;
	bool isActive;
};

