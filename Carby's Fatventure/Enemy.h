#pragma once
#include "Entity.h"
#include "AIBehavior.h"
#include <AEGraphics.h>

//Forward declaration 
class Room;
class Enemy :
	 public Entity
{
public:

	Enemy();
	Enemy(int level, int HP, int Atk, float Spd, int range, int width, float ATKCD, ENEMYTYPE BehaviorType);
	Enemy(Enemy &newEnemy);
	Enemy& operator=(Enemy& enemy);
	~Enemy();

	void InitRender();

	void setLevel(int level);
	void setRange(int Range, int Width);
	void setBehavior(ENEMYTYPE enemyType);
	void setNext(Enemy *nextEnemy);
	void setRoom(Room *curRoom);
	void setAtkCD(float newAtkCD);
	void setCurAtkCD(float newCurAtkCD);

	int getLevel();
	int getRange();
	int getWidth();
	ENEMYTYPE getBehavior();
	Enemy* Next();
	Room* getRoom();
	float getAtkCD();
	float getCurAtkCD();

	void Update(float deltaTime, Entity *target);
	void Draw();

protected:
	ENEMYTYPE behavior; /*This will decide their general attack  type*/
	Enemy *next; //This denotes the next enemy in list
	int level;
	int range; //Range of enemy
	int width; //Width of attack (IF it is AOE)
	float atkCD;
	float curAtkCD;
	Room *location;
	Node *nextNode; //Next node to go to. If it is not current Node, this is the next node to move to

	//For AEEngine
	AEGfxTexture *mobTexture;
	AEGfxVertexList *mobMesh;
};

