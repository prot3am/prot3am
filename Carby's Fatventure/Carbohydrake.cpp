#include "Carbohydrake.h"

Carbohydrake::Carbohydrake()
{
	hp = 100;
	isActive = false;
	InitState = ALERT;
	CurrentState = InitState;
	AtkCount = 0;

	*skillSlot = new Slot[4];
	(*skillSlot)[0] = Slot("Meatier Shower", 0, true, 0.0f, 3.0f, 1.0f);
	(*skillSlot)[1] = Slot("Toasty Tendies", 1, true, 1.5f, 3.0f, 2.0f);
	(*skillSlot)[2] = Slot("Carbonara Crush", 2, true, 0.5f, 1.0f, 1.0f);
	(*skillSlot)[3] = Slot("Seasoning Greetings", 3, true, 1.0f, 1.0f, 2.0f);
	curAtkDelay = 0.0f;
	curAtkDur = 0.0f;
	curAtkCD = 0.0f;
}

void Carbohydrake::Update(float deltaTime)
{
	if (isActive)
	{
		if (hp < 25)
		{
			ChangeState(ENRAGED);
		}

		//If preparing to attack
		if (curAtkDelay <= 0.0f)
		{
			if (CurrentState == ENRAGED)
				curAtkDelay -= deltaTime * 1.5f;
			else
				curAtkDelay -= deltaTime;
		}
		//If in the middle of an attack
		else if (curAtkDur > 0.0f)
		{
			//Continue Attack
			//Takes in previous attack, so need to decrement once
			if (AtkCount % 4 - 1 == 0)
			{
				MeatierShower();
			}
			else if (AtkCount % 4 - 1 == 1)
			{
				ToastyTendies();
			}
			else if (AtkCount % 4 - 1 == 2)
			{
				CarbonaraCrash();
			}
			else
			{
				SeasoningGreetings();
			}

			curAtkDur -= deltaTime;
		}
		else
		{
			if (CurrentState == ENRAGED)
				curAtkCD -= deltaTime * 1.5f;
			else
				curAtkCD -= deltaTime;

			if (curAtkCD < 0.0f)
			{
				//Decision Making here
				if (AtkCount % 4 == 0)
				{
					MeatierShower();
					curAtkDelay = skillSlot[0]->Delay();
					curAtkDur = skillSlot[0]->Duration();
					curAtkCD = skillSlot[0]->Cooldown();
					++AtkCount;
					counter = 6;
				}
				else if (AtkCount % 4 == 3)
				{
					SeasoningGreetings();
					curAtkDelay = skillSlot[3]->Delay();
					curAtkDur = skillSlot[3]->Duration();
					curAtkCD = skillSlot[3]->Cooldown();
					++AtkCount;
				}
				else
				{
					int decision = rand() % 2;
					if (decision)
					{
						ToastyTendies();
						curAtkDelay = skillSlot[1]->Delay();
						curAtkDur = skillSlot[1]->Duration();
						curAtkCD = skillSlot[1]->Cooldown();
						++AtkCount;
					}
					else
					{
						CarbonaraCrash();
						curAtkDelay = skillSlot[2]->Delay();
						curAtkDur = skillSlot[2]->Duration();
						curAtkCD = skillSlot[2]->Cooldown();
						++AtkCount;
					}
				}
			}

		}
	}
}

void Carbohydrake::MeatierShower()
{
	//Every 0.5s, shoot 1 more missle that sets a tile on fire
	if (curAtkDur < 0.5f * counter + 0.1f)
	{
		--counter;
	}
}

void Carbohydrake::ToastyTendies()
{
	//Uses InRange(Range/Width) to burn most of the ground. Does not set on fire.
	//Head follows player
	//If Player in that area, takes damage
}

void Carbohydrake::CarbonaraCrash()
{
	//Swipes from head's current position. Head follows player.
	//Uses Range only
}

void Carbohydrake::SeasoningGreetings()
{
	//Blows the player backwards. Does not do damage, but has a chance to make player step on burning tile
}

Carbohydrake::~Carbohydrake()
{
	delete[] *skillSlot;
}
