#pragma once
#include "Node.h"
#include "AEVec2.h"
#include "AEGraphics.h"
#include "Direction.h"
#include "RoomID.h"
#include "Enemy.h"
#include "Boss.h"
#include "Wall.h"
#include "Floor.h"

class Room
{
public:
	Room();
	Room(int x, int y); // to be changed based on read-in binary map
	~Room();

	void InitRender();
	//Update room and all inhabitants
	void Update(float deltaTime, Entity *player);
	//Draws room and all inhabitants
	void Draw();

	//Pathfinder related functions
	//Checks the node next to the chosen node at the center, in the direction specified. Returns true if it is not out of bounds
	bool checkSurroundingNodes(Node * centerN, DIRECTION dir);
	//Used for Pathfinder, but can be used to access other nodes next to current, in the direction specified
	Node *getSurroundingNode(Node *centerN, DIRECTION dir);
	//Pathfinder function to find a path from start to end via linked list
	Node *findPath(Node* startN, Node *endN);
	//Resets Parent Node for next findPath
	void resetPNodes();

	//Enemy detection
	//Melee units use inRange, Ranged units use hasLineOfSight
	bool inRange(Node *startN, Node *targetN, int range);
	//Melee units use this inRange if they have a full/block based attack
	bool inRange(Node *startN, Node *targetN, int range, int width, DIRECTION dir);
	//Uses Bresenham's Line Algorithm to check for nodes
	bool hasLineOfSight(Node *startN, Node *targetN);

	//Getter/Setter functions
	int mapX();
	int mapY();
	Node *getNode(int x, int y);
	void setDoors(int pos);
	bool getDoors(int pos);
	void setIdentity(roomID ID);
	roomID getIdentity();

	//For Enemy List Functions
	bool isEmpty();
	void AddEnemy(Enemy *newEnemy);
	void RemoveEnemy(int position);
	void SetEnemyActive(bool activeState);
	//void EnemyUpdate(float deltaTime, Enemy *actor, Entity *target);
	//virtual void BossUpdate(float deltaTime, Entity *target);

protected:
	
	Node GridNodes[13 * 9];
	int MapSizeX;
	int MapSizeY;
	bool doors[4] = { false };
	roomID identity;
	walltiles **walltest;
	floortiles *floor;

	AEGfxTexture *floorTexture;
	AEGfxTexture *cornerwallTexture; // for wall tiles at the 4 corners
	AEGfxTexture *midwallTexture;    // for wall tiles at center
	AEGfxTexture *leftsidewallTexture;   // for wall tiles at the sides of the center
	AEGfxTexture *rightsidewallTexture;   // for wall tiles at the sides of the center
	//AEGfxTexture *doorOpenTexture;   // for opened doors
	//AEGfxTexture *doorCloseTexture;  // for closed doors
	AEGfxVertexList *nodeMesh;

	//For Enemies
	Enemy *Template; //Move this to actual template when done
	Enemy *startEnemy;
	int enemyCount;
	Boss *boss;
};

