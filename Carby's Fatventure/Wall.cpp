#include "Wall.h"

walltiles::walltiles()
{
	setScale(1.0f);
	setVelocity(0, 0);
	setPositionCoord(10.0f, 10.0f);

	minmaxUpdate();

	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);
	wallTexture = AEGfxTextureLoad("..\\Assets\\TestWall.png");

	wallMesh = AEGfxMeshEnd();
}

void walltiles::drawwallgraphics(float x, float y)
{
	minmaxUpdate();

	AEGfxSetBlendMode(AE_GFX_BM_BLEND);
	AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);

	AEGfxSetPosition(x, y);
	AEGfxTextureSet(wallTexture, 0.0f, 0.0f);
	AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
	AEGfxSetTransparency(1.0f);
	AEGfxMeshDraw(wallMesh, AE_GFX_MDM_TRIANGLES);
}

walltiles::~walltiles()
{
	if(wallTexture != nullptr)
		AEGfxTextureUnload(wallTexture);

	AEGfxMeshFree(wallMesh);
	delete this;
}