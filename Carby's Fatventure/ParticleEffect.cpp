#include "ParticleEffect.h"

ParticleEffect::ParticleEffect()
{
	particleDir = AEVec2();
	particleDir.y = 1;
	particleAccel = 1.0f;
	particleSpd = 1.0f;
	particleDur = 2.0f;
	particleTimer = 0.0f;
	particlePos = AEVec2();
	active = false;
}

ParticleEffect::ParticleEffect(AEVec2 dir, float accel, float spd, float dur, AEVec2 pos)
{
	particleDir = dir;
	particleAccel = accel;
	particleSpd = spd;
	particleDur = dur;
	particleTimer = 0.0f;
	particlePos = pos;
	active = false;
}


ParticleEffect::~ParticleEffect()
{
}

bool ParticleEffect::isActive()
{
	return active;
}

bool ParticleEffect::checkActive()
{
	return particleTimer < particleDur;
}

void ParticleEffect::deactivate()
{
	active = false;
}

void ParticleEffect::SetParams(AEVec2 dir, float accel, float spd, float dur, AEVec2 pos)
{
	particleDir = dir;
	particleAccel = accel;
	particleSpd = spd;
	particleDur = dur;
	particleTimer = 0.0f;
	particlePos = pos;
	active = true;
}

void ParticleEffect::Update(float dt)
{
	particleSpd += particleAccel * dt;
	particlePos.x += particleSpd * particleDir.x;
	particlePos.y += particleSpd * particleDir.y;
	particleTimer += dt;
	//Delete is done in the particle effect system to keep count
	//if (particleTimer > particleDur)
		//delete this;
}