#include "Node.h"

void Node::SetCoordinates(int x, int y)
{
	xCord = x;
	yCord = y;
}

void Node::SetParentNode(int x, int y)
{
	pXCord = x;
	pYCord = y;
}

void Node::SetAStarValues(float val1, float val2)
{
	g = val1;
	h = val2;
	f = g + h;
}

void Node::SetNODETYPE(NODETYPE walk)
{
	walkable = walk;
}

void Node::MoveToOpenList()
{
	inOpenList = true;
}

void Node::MoveToClosedList()
{
	inClosedList = true;
	inOpenList = false;
}

void Node::MoveOutOfList()
{
	inClosedList = false;
	inOpenList = false;
}

NODETYPE Node::GetWalkable()
{
	return walkable;
}

int Node::GetXCord()
{
	return xCord;
}

int Node::GetYCord()
{
	return yCord;
}

float Node::GetFVal()
{
	return f;
}

float Node::GetGVal()
{
	return g;
}

float Node::GetHVal()
{
	return h;
}

int Node::GetParentX()
{
	return pXCord;
}

int Node::GetParentY()
{
	return pYCord;
}

int Node::GetCurrentList()
{
	if (inClosedList)
		return 2;
	else if (inOpenList)
		return 1;
	else
		return 0;
}

void Node::SetTileType(TILETYPE tiletype)
{
	tileType = tiletype;
}

AEMtx33& Node::GetTransformMtx()
{
	return transformMtx;
}

TILETYPE Node::GetTileType() const
{
	return tileType;

}

Node::Node()
{
	SetAStarValues(0, 0);
	SetCoordinates(-1, -1);
	SetParentNode(-1, -1);
	SetNODETYPE(WALKABLE);
	SetTileType(FLOOR);   //default setting
	MoveOutOfList();
}

Node::Node(int x, int y)
{
	SetAStarValues(0, 0);
	SetCoordinates(x, y);
	SetParentNode(-1, -1);
	SetNODETYPE(WALKABLE);
	SetTileType(FLOOR);  //default setting
	MoveOutOfList();
}

Node::~Node()
{
	//delete this;
}