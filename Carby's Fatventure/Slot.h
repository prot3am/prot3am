#pragma once
#include <iostream>
#include <ctime>
#include <fstream>
#include <string>
using std::string;
using namespace std;

class Slot
{
public:
	Slot();
	Slot(string name, int id, bool active, float delay, float duration, float cooldown);
	~Slot();

	string Name();
	bool isActive();
	int ID();

	void setName(string newName);
	void setActive(bool isActive);
	void setID(int newID);
	void setValues(float del, float dur, float cd);
	void setRange(int range, int width);

	float Delay();
	float Duration();
	float Cooldown();
	int Range();
	int Width();

	//void ReadFromFile(int SlotType);

protected:
	string name;
	int id;
	bool active;

	float delay;
	float duration;
	float cooldown;

	int range;
	int width;
};

