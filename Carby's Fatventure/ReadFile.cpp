#include "pch.h"
//#include "stdfx.h"

#include <fstream>
#include <iostream>
//#include "Enemy.h"
#include "ReadFile.h"
#include "Slot.h"

//int HP, int Atk, float Spd, int IdleState, int BehaviorType, float DetectionRadius, int DetectionType;



 void ReadFileEnemy::ReadInEnemyData()
{
	string name; //name of the enemy
	string Extra; //for extracting words
	int HP = 0, Atk = 0, Spd = 0, behaviour = 0, direction = 0, level = 0, range = 0, width = 0, atkCD = 0;

	std::ifstream EnemyReadFile("Data/Enemy.txt", std::ifstream::in);

	if (EnemyReadFile.is_open())
	{
		while (!EnemyReadFile.eof())
		{
			EnemyReadFile >> Extra >> name
						  >> Extra >> HP
						  >> Extra >> Atk
					      >> Extra >> Spd
						  >> Extra >> behaviour
						  >> Extra >> direction
						  >> Extra >> level
						  >> Extra >> range
						  >> Extra >> width
						  >> Extra >> atkCD;

			Extra = ""; //get rid of the string in Extra to save space
		}
	}
	else
	{
		cout << "Unable to open Enemy.txt file." << endl;
		return;
	}

	//set all the values
	setName(name);
	setHP(HP);
	setAtk(Atk);
	setSpd(Spd);
	setBehaviour(behaviour);
	setDirection(direction);
	setLevel(level);
	setRange(range);
	setWidth(width);
	setAtkCD(atkCD);
}


 //This function is just for testing to see if the getName, getAtk etc.. functions work.
 //You can just remove this however you see fit :)
 void testing_getName(ReadFileEnemy &enemy)
 {
	 string hoho;
	 int HP = 0, Atk = 0, Spd = 0, behaviour = 0, direction = 0, level = 0,
		 range = 0, width = 0, atkCD = 0;

	 //getting the values of the enemy
	 hoho = enemy.getName();
	 HP = enemy.getHP();
	 Atk = enemy.getAtk();
	 Spd = enemy.getSpd();
	 behaviour = enemy.getBehaviour();
	 direction = enemy.getDirection();
	 level = enemy.getLevel();
	 range = enemy.getRange();
	 width = enemy.getWidth();
	 atkCD = enemy.getAtkCD();

	 //printing out in progress...
	 std::cout << "getName: " << hoho << std::endl;
	 std::cout << "mainHP: " << HP << std::endl;
	 std::cout << "mainAtk: " << Atk << std::endl;
	 std::cout << "mainSpd: " << Spd << std::endl;
	 std::cout << "mainBehaviour: " << behaviour << std::endl;
	 std::cout << "mainDirection: " << direction << std::endl;
	 std::cout << "mainLevel: " << level << std::endl;
	 std::cout << "mainRange: " << range << std::endl;
	 std::cout << "mainWidth: " << width << std::endl;
	 std::cout << "mainAtkCD: " << atkCD << std::endl;
 }
 

 void loadsMilstoneAchievement()
 {
	 string Extra;
	 int EnemyKilled = 0, DamageDone = 0, DamageRecieved = 0, GoldRecieved = 0,
		 UpgradesDone = 0, EnergyCost = 0;
	 ifstream fileRead("Data/milstones.txt", ifstream::in);

	 fileRead >> Extra >> EnemyKilled
		 >> Extra >> DamageDone
		 >> Extra >> DamageRecieved
		 >> Extra >> GoldRecieved
		 >> Extra >> UpgradesDone
		 >> Extra >> EnergyCost;
	 Extra = "";
 }

 ReadFileEnemy::ReadFileEnemy(string Name, int HP, int Atk, int Spd, int behaviour, int Direction,
	 int Level, int Range, int Width, int AtkCD)
 {
	 EnemyName = Name;
	 EnemyHP = HP;
	 EnemyAtk = Atk;
	 EnemySpd = Spd;
	 EnemyBehaviour = behaviour;
	 EnemyDirection = Direction;
	 EnemyLevel = Level;
	 EnemyRange = Range;
	 EnemyWidth = Width;
	 EnemyAtkCD = AtkCD;
 };

 //get name data from file
 void ReadFileEnemy::setName(string name)
 {
	 EnemyName = name;
 }
 string ReadFileEnemy::getName()
 {
	 return EnemyName;
 }

 //get HP data from file
 void ReadFileEnemy::setHP(int hp)
 {
	 EnemyHP = hp;
 }
 int ReadFileEnemy::getHP()
 {
	 return EnemyHP;
 }

 //get Atk data from file
 void ReadFileEnemy::setAtk(int atk)
 {
	 EnemyAtk = atk;
 }
 int ReadFileEnemy::getAtk()
 {
	 return EnemyAtk;
 }

 //get Spd data from file
 void ReadFileEnemy::setSpd(int x)
 {
	 EnemySpd = x;
 }
 int ReadFileEnemy::getSpd()
 {
	 return EnemySpd;
 }

 //get behaviour data from file
 void ReadFileEnemy::setBehaviour(int behaviour)
 {
	 EnemyBehaviour = behaviour;
 }
 int ReadFileEnemy::getBehaviour()
 {
	 return EnemyBehaviour;
 }

 //get Direction data from file
 void ReadFileEnemy::setDirection(int direction)
 {
	 EnemyDirection = direction;
 }
 int ReadFileEnemy::getDirection()
 {
	 return EnemyDirection;
 }

 //get Level data from file
 void ReadFileEnemy::setLevel(int level)
 {
	 EnemyLevel = level;
 }
 int ReadFileEnemy::getLevel()
 {
	 return EnemyLevel;
 }

 //get Range data from file
 void ReadFileEnemy::setRange(int range)
 {
	 EnemyRange = range;
 }
 int ReadFileEnemy::getRange()
 {
	 return EnemyRange;
 }

 //get Width data from file
 void ReadFileEnemy::setWidth(int width)
 {
	 EnemyWidth = width;
 }
 int ReadFileEnemy::getWidth()
 {
	 return EnemyWidth;
 }

 //get AtkCD data from file
 void ReadFileEnemy::setAtkCD(int atkCD)
 {
	 EnemyAtkCD = atkCD;
 }
 int ReadFileEnemy::getAtkCD()
 {
	 return EnemyAtkCD;
 }


 Milestone::Milestone()
 {
	 string Extra;
	 ifstream fileRead("Data/milstones.txt", ifstream::in);

	 fileRead >> Extra >> EnemyKilled
		 >> Extra >> DamageDone
		 >> Extra >> DamageRecieved
		 >> Extra >> GoldRecieved
		 >> Extra >> UpgradesDone
		 >> Extra >> EnergyCost;
	 Extra = "";
 }

int Milestone::getEnemyKilled()
 {
	return EnemyKilled;
 }

int Milestone::getDamageDone()
 {
	return DamageDone;
 }

int Milestone::getDamageRecieved()
{
	return DamageRecieved;
}

int Milestone::getGoldRecieved()
{
	return GoldRecieved;
}

int Milestone::getUpgradesDone()
{
	return UpgradesDone;
}

int Milestone::getEnergyCost()
{
	return EnergyCost;
}