#pragma once
#include "Room.h"
#include <iostream>
#include "EnemyList.h"

//This file is to test A Star Pathfinder
int main()
{
	/*Declaring Nodes*/

	Room *room = new Room(16,16);

	Node *pathNode;
	Node *startNode = room->getNode(1,1);
	Node *endNode = room->getNode(8,8);

	/*room->getNode(2,2)->SetWalkable(false);
	room->getNode(3,3)->SetWalkable(false);
	room->getNode(4,4)->SetWalkable(false);
	room->getNode(5,5)->SetWalkable(false);
	room->getNode(6,6)->SetWalkable(false);
	room->getNode(3,4)->SetWalkable(false);
	room->getNode(3,5)->SetWalkable(false);
	room->getNode(3,6)->SetWalkable(false);
	room->getNode(4,3)->SetWalkable(false);
	room->getNode(5,3)->SetWalkable(false);
	room->getNode(6,3)->SetWalkable(false);*/

	pathNode = room->findPath(startNode, endNode);

	while (room->getNode(pathNode->GetXCord(), pathNode->GetYCord())->GetParentX() != -1)
	{
		std::cout << pathNode->GetXCord() << " " << pathNode->GetYCord() << std::endl;

		if (room->inRange(startNode, pathNode, 4))
			std::cout << "In range with distance: " << pathNode->GetXCord() - startNode->GetXCord() << " and "
			<< pathNode->GetYCord() - startNode->GetYCord() << std::endl;

		if (room->inRange(startNode, pathNode, 4, 3, BTMRIGHT))
			std::cout << "In range towards right AOE with distance: " << pathNode->GetXCord() - startNode->GetXCord() << " and "
			<< pathNode->GetYCord() - startNode->GetYCord() << std::endl;

		if (room->hasLineOfSight(startNode, pathNode))
			std::cout << "Has Line of Sight at: " << pathNode->GetXCord() << " and " << pathNode->GetYCord() << std::endl;

		pathNode = room->getNode(pathNode->GetParentX(),pathNode->GetParentY());
	}
	std::cout << pathNode->GetXCord() << " " << pathNode->GetYCord() << std::endl;

	EnemyList theList = EnemyList();
	Enemy *newEnemy = new Enemy();
	
	theList.Add(newEnemy);
	theList.Add(newEnemy);

	while (!theList.isEmpty())
	{
		std::cout << "Deleting" << std::endl;
		theList.Remove(0);
	}

	//Endless loop just to keep console open
	while (1);

     return 0;
}