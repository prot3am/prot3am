#pragma once

enum ENEMYTYPE
{
	MELEE = 0,
	MELEEAOE,
	RANGE
};
enum BossState
{
	ALERT = 0,
	ENRAGED,
	DOCILE
};