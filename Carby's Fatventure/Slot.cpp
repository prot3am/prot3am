#include "Slot.h"



Slot::Slot()
{
	name = "default";
	id = -1;
	active = false;
	delay = 1.0f;
	duration = 2.0f;
	cooldown = 3.0f;
}

Slot::Slot(string name, int id, bool active, float delay, float duration, float cooldown)
{
	this->name = name;
	this->id = id;
	this->active = active;
	this->delay = delay;
	this->duration = duration;
	this->cooldown = cooldown;
}

string Slot::Name()
{
	return name;
}

bool Slot::isActive()
{
	return active;
}

int Slot::ID()
{
	return id;
}

void Slot::setName(string newName)
{
	name = newName;
}
void Slot::setActive(bool isActive)
{
	active = isActive;
}
void Slot::setID(int newID)
{
	id = newID;
}

void Slot::setValues(float del, float dur, float cd)
{
	delay = del;
	duration = dur;
	cooldown = cd;
}

void Slot::setRange(int Range, int Width)
{
	range = Range;
	width = Width;
}

float Slot::Delay()
{
	return delay;
}

float Slot::Duration()
{
	return duration;
}

float Slot::Cooldown()
{
	return cooldown;
}

int Slot::Range()
{
	return range;
}

int Slot::Width()
{
	return width;
}

/*void Slot::ReadFromFile(int SlotType)
{
	ifstream file;
	string fullText;

	/*if (SlotType == 0)
	{
		file.open("AtkSlots.txt");
		while (!file.eof)
		{
			getline(file, fullText);
		}
	}
	else if (SlotType == 1)
	{
		file.open("SkillSlots.txt");
		while (!file.eof)
		{
			getline(file, fullText);
		}
	}
	else
	{
		file.open("GenSlots.txt");
		while (!file.eof)
		{
			getline(file, fullText);
		}
	}
}*/

Slot::~Slot()
{
}
