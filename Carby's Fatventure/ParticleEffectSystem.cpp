#include "ParticleEffectSystem.h"
#include <cmath>

ParticleEffectSystem::ParticleEffectSystem()
{
	isSpawning = true;
	isActive = true;
	isViewable = true;
	varianceDir = 0.0f;
	variancePos = 0.0f;
	duration = 5.0f;
	durTimer = 0;
	spawnRate = 0.2f;
	spawnTimer = 0.0f;
	particleMaxCount = 25;
	particleCurCount = 0;
	particlePos = AEVec2();
	particleEffType = SPREAD;
	particles = new ParticleEffect[particleMaxCount];

	particleSpd = 0.0f;
	particleAccel = 1.0f;
	particleDur = 3.0f;
	particleDir = AEVec2();
	particleDir.y = 1.0f;
}

ParticleEffectSystem::ParticleEffectSystem(float varDir, float varPos, float dur, float rate, int max, EFFECT_TYPE eff, float pSpd, float pAccel, float pDur, AEVec2 pDir, AEVec2 pos)
{
	isSpawning = true;
	isActive = true;
	isViewable = true;
	varianceDir = varDir;
	variancePos = varPos;
	duration = dur;
	durTimer = 0;
	spawnRate = rate;
	spawnTimer = 0.0f;
	particleMaxCount = max;
	particleCurCount = 0;
	particlePos = pos;
	particleEffType = eff;
	particles = new ParticleEffect[particleMaxCount];

	particleSpd = pSpd;
	particleAccel = pAccel;
	particleDur = pDur;
	particleDir = pDir;
}

ParticleEffectSystem::~ParticleEffectSystem()
{
	delete[] particles;
}

void ParticleEffectSystem::Update(float dt)
{
	if (isActive)
	{
		if (isSpawning && particleCurCount < particleMaxCount)
		{
			//Spawn Code here
			if (spawnTimer > spawnRate)
			{
				//Getting an available node
				int i = 0;
				for (; i < particleMaxCount; ++i)
				{
					if (!particles[i].isActive())
						break;
				}

				//If there is a spot found
				if (i < particleMaxCount + 1)
				{
					//TODO set variance
					particles[i].SetParams(particleDir, particleAccel, particleSpd, particleDur, particlePos);
					++particleCurCount;
					spawnTimer -= spawnRate;
				}
			}
			//Do this no matter what
			spawnTimer += dt;
		}

		//Updating the particle system here, based on particle effect type
		//TODO
		//Check if Particle System is still active
		//Change 

	}

	//Updating the particles all the time (so no fizzle)
	for (int i = 0; i < particleMaxCount; ++i)
	{
		if (particles[i].isActive())
		{
			particles[i].Update(dt);
			if (!particles[i].checkActive())
				particles[i].deactivate();
		}
	}
}

void ParticleEffectSystem::Draw()
{

}

void ParticleEffectSystem::TurnOff()
{
	isSpawning = false;
	isActive = false;
	isViewable = false;
}

//1 for Spawn, 2 for Active, 3 for Viewable
void ParticleEffectSystem::TurnOff(int i)
{
	switch (i)
	{
	case 1: isSpawning = false;
	case 2: isActive = false;
	case 3: isViewable = false;
	default: isViewable = false;
	};
}

void ParticleEffectSystem::TurnOn()
{
	isSpawning = true;
	isActive = true;
	isViewable = true;
}

//1 for Spawn, 2 for Active, 3 for Viewable
void ParticleEffectSystem::TurnOn(int i)
{
	switch (i)
	{
	case 1: isSpawning = true;
	case 2: isActive = true;
	case 3: isViewable = true;
	default: isViewable = true;
	};
}

void ParticleEffectSystem::setVarDir(float var)
{
	if (var > 1.0f)
		varianceDir = 1.0f;
	else
		varianceDir = var;
}

void ParticleEffectSystem::setVarPos(float var)
{
	variancePos = var;
}

void ParticleEffectSystem::setDur(float dur)
{
	duration = dur;
}

void ParticleEffectSystem::resetDurTimer()
{
	durTimer = 0.0f;
}

void ParticleEffectSystem::setRate(float newSRate)
{
	spawnRate = newSRate;
}

void ParticleEffectSystem::resetSpawnTimer()
{
	spawnTimer = 0.0f;
}

void ParticleEffectSystem::setMaxParticleCount(int maxCount)
{
	particleMaxCount = maxCount;
}

void ParticleEffectSystem::resetCurParticleCount()
{
	particleMaxCount = 0;
}

void ParticleEffectSystem::setParticlePos(AEVec2 newPos)
{
	particlePos = newPos;
}

void ParticleEffectSystem::setParticleEff(EFFECT_TYPE newEff)
{
	particleEffType = newEff;
}

void ParticleEffectSystem::setParticleSpd(float newSpd)
{
	particleSpd = newSpd;
}

void ParticleEffectSystem::setParticleAccel(float newAccel)
{
	particleAccel = newAccel;
}

void ParticleEffectSystem::setParticleDur(float newDur)
{
	particleDur = newDur;
}

void ParticleEffectSystem::setParticleDir(AEVec2 newDir)
{
	particleDir = newDir;
}

