#pragma once

#include "AEEngine.h"
#include "CollisionBox.h"

class floortiles :
	public hitBox
{
public:
	floortiles();
	floortiles(float posX, float posY);
	~floortiles();
	void drawfloorGraphics(int mapsizeX, int mapsizeY);

protected:
	AEGfxVertexList *tileMesh;
	AEGfxTexture *floorTexture;
};
