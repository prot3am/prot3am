#include "Enemy.h"
#include "Room.h"

Enemy::Enemy()
{
	//From Entity
	setName("Enemy");
	currentNode = nullptr;
	direction = BTM; //Downwards by default
	setStats(2, 3, 4);
	setDef(5); //Def is not necessarily used for most entities
	sizeX = 1;
	sizeY = 1;
	isActive = true;

	//From Enemy
	behavior = MELEE;
	next = nullptr;
	setLevel(1);
	setRange(5, 6);
	atkCD = 2.0f;
	curAtkCD = 0.0f;
}

/*Does not actually position the Enemy*/
Enemy::Enemy(int level, int HP, int Atk, float Spd, int Range, int Width, float ATKCD, ENEMYTYPE behaviorType)
{
	//From Entity
	setName("Enemy");
	currentNode = nullptr;
	direction = BTM; //Downwards by default
	setStats(HP, Atk, Spd);
	setDef(0); //Def is not necessarily used for most entities
	sizeX = 1; //Size of Entity, not attack
	sizeY = 1; //Size of Entity, not attack
	isActive = true;

	//From Enemy
	behavior = behaviorType;
	next = nullptr;
	setLevel(level);
	setRange(Range, Width);
	atkCD = ATKCD;
	curAtkCD = 0.0f;
	nextNode = nullptr;
}

Enemy::Enemy(Enemy &newEnemy)
{
	//From Entity
	setName(newEnemy.name);
	currentNode = newEnemy.currentNode;
	direction = newEnemy.direction; //Downwards by default
	setStats(newEnemy.hp, newEnemy.atk, newEnemy.spd);
	setDef(newEnemy.def); //Def is not necessarily used for most entities
	sizeX = newEnemy.sizeX; //Size of Entity, not attack
	sizeY = newEnemy.sizeY; //Size of Entity, not attack
	isActive = false;

	//From Enemy
	next = newEnemy.next;
	setLevel(newEnemy.level);
	setRange(newEnemy.range, newEnemy.width);
	atkCD = newEnemy.atkCD;
	curAtkCD = 0.0f;
	behavior = newEnemy.behavior;
	nextNode = currentNode;
}

Enemy& Enemy::operator=(Enemy& newEnemy)
{
	if (next)
		delete next;
	//From Entity
	setName(newEnemy.name);
	currentNode = newEnemy.currentNode;
	direction = newEnemy.direction; //Downwards by default
	setStats(newEnemy.hp, newEnemy.atk, newEnemy.spd);
	setDef(newEnemy.def); //Def is not necessarily used for most entities
	sizeX = newEnemy.sizeX; //Size of Entity, not attack
	sizeY = newEnemy.sizeY; //Size of Entity, not attack
	isActive = false;

	//From Enemy
	next = nullptr;
	setLevel(newEnemy.level);
	setRange(newEnemy.range, newEnemy.width);
	atkCD = newEnemy.atkCD;
	curAtkCD = 0.0f;
	behavior = newEnemy.behavior;

	return *this;
}

Enemy::~Enemy()
{
	AEGfxMeshFree(mobMesh);
	AEGfxTextureUnload(mobTexture);
	delete next;
}

/*bool Enemy::Detect(Node *player)
{
	bool detected = false;

	switch (behavior)
	{
	case MELEE:
		if (inRange(currentNode, player, range))
		{
			detected = true;
		}
		break;
	case MELEEAOE:
		if (inRange(currentNode, player, range, width, direction))
		{
			detected = true;
		}
		break;
	case RANGE:
		if (hasLineOfSight(currentNode, player))
		{
			detected = true;
		}
		break;
	default:
		break;
	}
	return detected;
}*/

void Enemy::InitRender()
{
	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);

	mobMesh = AEGfxMeshEnd();

	//Move this to templating after
	mobTexture = AEGfxTextureLoad("..\\Assets\\basicenemy.png");
}

void Enemy::setBehavior(ENEMYTYPE type)
{
	behavior = type;
}

void Enemy::setRange(int Range, int Width)
{
	range = Range;
	width = Width;
}

void Enemy::setLevel(int Level)
{
	level = Level;
}

void Enemy::setNext(Enemy *nextEnemy)
{
	next = nextEnemy;
}

void Enemy::setAtkCD(float val)
{
	atkCD = val;
}

void Enemy::setCurAtkCD(float val)
{
	curAtkCD = val;
}

void Enemy::setRoom(Room *curRoom)
{
	location = curRoom;
}

Enemy* Enemy::Next()
{
	return next;
}

int Enemy::getLevel()
{
	return level;
}

int Enemy::getRange()
{
	return range;
}

int Enemy::getWidth()
{
	return width;
}

ENEMYTYPE Enemy::getBehavior()
{
	return behavior;

}

float Enemy::getAtkCD()
{
	return atkCD;
}

float Enemy::getCurAtkCD()
{
	return curAtkCD;
}

Room* Enemy::getRoom()
{
	return location;
}

void Enemy::Update(float deltaTime, Entity *target)
{
	if (isActive)
	{
		switch (behavior)
		{
		case MELEE:
			if (location->inRange(currentNode, target->getNode(), range))
			{
				if (curAtkCD < 0.0f)
				{
					//Attack Target
					curAtkCD = atkCD * CLOCKS_PER_SEC;
					target->takeDamage(atk);
				}
			}
			else
			{
				//Run to player
			}
			curAtkCD -= deltaTime;
			break;
		case MELEEAOE:
			if (location->inRange(currentNode, target->getNode(), range, width, direction))
			{
				if (curAtkCD < 0.0f)
				{
					//Attack Target
					curAtkCD = atkCD * CLOCKS_PER_SEC;
					target->takeDamage(atk);
				}
			}
			else
			{
				//Run to player
			}
			curAtkCD -= deltaTime;
			break;
		case RANGE:
			if (location->hasLineOfSight(currentNode, target->getNode()))
			{
				if (curAtkCD < 0.0f)
				{
					//Attack Target
					curAtkCD = atkCD * CLOCKS_PER_SEC;
					target->takeDamage(atk); //This will be changed to spawning bullets
				}
			}
			curAtkCD -= deltaTime;
			break;
		default:
			break;
		}
	}
}

void Enemy::Draw()
{
	AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
	AEGfxSetBlendMode(AE_GFX_BM_BLEND);
	AEGfxSetPosition(positionX, positionY);
	AEGfxTextureSet(mobTexture, 0.0f, 0.0f);
	AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
	AEGfxSetTransparency(1.0f);
	AEGfxMeshDraw(mobMesh, AE_GFX_MDM_TRIANGLES);
}