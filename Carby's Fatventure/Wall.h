#pragma once

#include "AEEngine.h"
#include "CollisionBox.h"

class walltiles :
	public hitBox
{
public:
	walltiles();
	~walltiles();
	void drawwallgraphics(float x, float y);

protected:
	AEGfxVertexList *wallMesh;
	AEGfxTexture *wallTexture;
};
