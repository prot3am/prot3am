#pragma once

enum GS_STATES
{
	GS_TESTLEVEL = 0,
	GS_FIRE,
	GS_FOREST,
	GS_LAKE,

	GS_SPLASH,
	GS_MAIN,
	GS_SHOP,
	GS_QUIT,
	GS_RESTART
};