/* Start Header ************************************************************************/
/*!
\file GameStateManager.cpp
\author Teh Yu Yin, t.yuyin, 390005118
\par t.yuyin\@digipen.edu)
\date 23/01/2019
\brief
This file contains the functions for initializing and updating the game state manager.

Copyright (C) 2019 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents
without the prior written consent of DigiPen Institute of
Technology is prohibited.
*/
/* End Header **************************************************************************/

#include "GameStateManager.h"
#include "GameStateList.h"
#include "TestLevel.h"
#include <iostream>
#include <fstream>

int _current = 0, _previous = 0, _next = 0;  // current game state status

FP fpLoad = 0, fpInit = 0, fpDraw = 0, fpFree = 0, fpUnload = 0; // function pointers

FPU fpUpdate = 0;
// ----------------------------------------------------------------------------
// This function sets up the game state manager
// It should be called once after system initialization
// It starts up a game state upon initialization
// ----------------------------------------------------------------------------
void GSM_Initialize(int startingState)
{
	_current = _previous = _next = startingState;
}

// ----------------------------------------------------------------------------
// This function updates the game states of the game state manager
// It should be called for as long as the game runs
// It checks and updates function pointers accordingly
// ----------------------------------------------------------------------------
void GSM_Update()
{
	switch (_current)
	{
	case GS_TESTLEVEL:   // if test level is selected
		fpLoad = TestLevel_Load;
		fpInit = TestLevel_Init;
		fpUpdate = TestLevel_Update;
		fpDraw = TestLevel_Draw;
		fpFree = TestLevel_Free;
		fpUnload = TestLevel_Unload;
		break; 

	//case GS_FOREST:   // if forest level is selected
	//	fpLoad = LevelForest_Load;
	//	fpInit = LevelForest_Init;
	//	fpUpdate = LevelForest_Update;
	//	fpDraw = LevelForest_Draw;
	//	fpFree = LevelForest_Free;
	//	fpUnload = LevelForest_Unload;
	//	break;

	//case GS_FIRE:   // if fire level is selected
	//	fpLoad = LevelFire_Load;
	//	fpInit = LevelFire_Init;
	//	fpUpdate = LevelFire_Update;
	//	fpDraw = LevelFire_Draw;
	//	fpFree = LevelFire_Free;
	//	fpUnload = LevelFire_Unload;
	//	break;

	//case GS_LAKE:   // if lake level is selected
	//	fpLoad = LevelLake_Load;
	//	fpInit = LevelLake_Init;
	//	fpUpdate = LevelLake_Update;
	//	fpDraw = LevelLake_Draw;
	//	fpFree = LevelLake_Free;
	//	fpUnload = LevelLake_Unload;
	//	break;

	//case GS_SPLASH:   // if splash screen is selected
	//	fpLoad = Splash_Load;
	//	fpInit = Splash_Init;
	//	fpUpdate = Splash_Update;
	//	fpDraw = Splash_Draw;
	//	fpFree = Splash_Free;
	//	fpUnload = Splash_Unload;
	//	break;

	//case GS_SHOP:   // if shop screen is selected
	//	fpLoad = Shop_Load;
	//	fpInit = Shop_Init;
	//	fpUpdate = Shop_Update;
	//	fpDraw = Shop_Draw;
	//	fpFree = Shop_Free;
	//	fpUnload = Shop_Unload;
	//	break;

	case GS_QUIT:    // if quit is selected
		_current = GS_QUIT;
		break;
	case GS_RESTART: // if level has been restarted
		_previous = _current;
		_current = _next;
		break;
	default:
		std::cout << "INVALID STATE SELECTED, PLEASE RESTART" << std::endl;
		break;
	}
}