#include "Map.h"

//Function to generate map for the levels
Map::Map( int in_width,  int in_height, unsigned int max)
{
	width = in_width;
	height = in_height;
	rooms = new Room[width*height];
	maxRooms = max;
	currentRoom = nullptr;
}

//FUnction to set level start, boss and enemy rooms
void Map::setRooms()
{
	std::random_device rd;
	std::mt19937 gen(rd());

	int random =  gen() % (width*height - 1);//For all rooms
	int prevNum;
	//Setting random start room
	 rooms[random].setIdentity(START);
	 //Setting current room pointer to start room, to be updated every room change.
	 currentRoom = &(rooms[random]);

	prevNum = random;
	//Guard against overwriting start room
	while (prevNum == random)
	{
		random = gen() % (width*height - 1);
	}
	//Setting random Boss room
	 rooms[random].setIdentity(BOSS);


	for (int i = 0; i < height;i++)
	{
		for (int j = 0;j < width;j++)
		{
			//To prevent multiple boss rooms and starting rooms
			if (rooms[i*width + j].getIdentity() != START || rooms[i*width + j].getIdentity() != BOSS)
				 rooms[i*width+j].setIdentity(ENEMY);
		}
	}

}

//Function to connect rooms together
void Map::room_Connectors()
{
	for (int i = 0; i < height;i++)
	{
		for (int j = 0; j < width;j++)
		{
			//Check for adj rooms in 4 cardinal directions
			//Checking Upwards
			if (checkRooms( j, i, 0, -1))
				rooms[i*width+j].setDoors(0);
			//Checking Right
			if (checkRooms( j, i, 1, 0))
				rooms[i*width + j].setDoors(1);
			//Checking Down
			if (checkRooms( j, i, 0, 1))
				rooms[i*width + j].setDoors(2);
			//Checking Left
			if (checkRooms( j, i, -1, 0))
				rooms[i*width + j].setDoors(3);
		}

	}
}

bool Map::checkRooms( int posX, int posY, int moveX, int moveY)
{
	
	posX += moveX;
	posY += moveY;
	//Guard against checker going out of grid boundary and to check for existing rooms
	while(posY > -1 && posX > -1 && posX < width && posY < height )
	{
		if(rooms[posY*width+posX].getIdentity()!=NONE)
		return true;
	}
	return false;
}

//Function to randomly remove excess rooms from Map grid
/*void Map::remExcessRooms()
{
	int totalRooms=width*height;
	//while total rooms > maxRooms
	while(totalRooms>maxRooms)
	{
	std::random_device rd;
	std::mt19937 gen(rd());

	int random =  gen() % (width*height - 1)
	if(rooms[random].getIdentity == ENEMY)
		{
		//Check if rooms have more than 2 connectors
		int total=0;
			for(int count=0;count<4;count++)
			{
			
				if(rooms[random].doors[count])
					total++;
			}
			if(total>2)
			{
				rooms[random].setIdentity(NONE);
				totalRooms--;
			}

		}
	}
}*/

void Map::setCurrentRoom(Room* newRoom)
{
	currentRoom = newRoom;
}

Room* Map::getCurrentRoom()
{
	return currentRoom;
}

//Function to destroy map once level is over and not restarted
 Map::~Map()
{
	delete[] rooms;
	rooms = nullptr;
}

 void Map::CreateEnemy()
 {
	 //Checking each room in map
	 for (int count = 0;count < (width*height);count++)
	 {
		 //Checks if room is an enemy room
		 if (rooms[count].getIdentity() == ENEMY)
		 {
			
			 for (int repeat = 0;repeat < 3;)
			 {
				 std::random_device rd;
				 std::mt19937 gen(rd());
				 //For random posX in room
				 int randomX = gen() % (16);

				 //For random posY in room
				 int randomY = gen() % (16);
				 //Check if spawn node is walkable
				 Node* spawnNode = rooms[count].getNode(randomX, randomY);
				 if (spawnNode->GetWalkable())
				 {
					 //Generate new enemy
					 Enemy * newEnemy = new Enemy(1, 1, 1, 1.0f, 1, 1, 1.0f, MELEE);
					 //Assign position to spawn node
					 newEnemy->setPosition(currentRoom->getNode(randomX, randomY));

					 //Update respective room enemy list
					 currentRoom->AddEnemy(newEnemy);

					 repeat++;
				 }
			 }
		 }
		 //Spawn boss
		 /*
		 if (rooms[count].getIdentity() == BOSS)
		 {
			Enemy* newEnemy= new Enemy(2, 5, 2, 1.0f, 1, 1, 1.5f, MELEE);
			newEnemy->setPosition(8,8);

			EnemyList * enemyList = rooms[count].getEnemyList();
			enemyList->Add(newEnemy);
		 }*/
	 }
 }