#include "Player.h"

Player::Player()
{
	setLengths(20.0f,20.0f);
	setStats(3, 1, 1.0f);
	maxHP = 3;
	energy = 15;
	maxEnergy = 15;

	setPositionCoord(50.f, 10.0f);
	setScale(1.0f);
	setVelocity(5.0f, 6.0f);

	isActive = true;

	ticks = 0;

	speedmod = 0.05f;

	InitRender();
}

Player::Player(int HP, int maxhp, int attack, float speed, int defense, int mana, int maxmana)
{
	setStats(HP, attack, speed);
	maxHP = maxhp;
	energy = mana;
	maxEnergy = maxmana;
	def = defense;

	setPositionCoord(50.f, 10.0f);
	setScale(1.0f);
	setVelocity(5.0f, 6.0f);

	isActive = true;

	ticks = 0;

	speedmod = 0.05f;

	InitRender();
}

Player::~Player()
{
	AEGfxTextureUnload(PlayerTexture);
	AEGfxTextureUnload(AbilityBar);
	AEGfxTextureUnload(HealthIcon);
	AEGfxTextureUnload(EnergyIcon);
	AEGfxMeshFree(PlayerMesh);
	AEGfxMeshFree(SlotBarMesh);
	AEGfxMeshFree(ResourceMesh);
	AEGfxMeshFree(ResourceMeshHalf);
	AEGfxMeshFree(ResourceMeshThird);
}

void Player::InitRender()
{
	AEGfxMeshStart();
	AEGfxTriAdd(-32.0f, -32.0f, 0x00FFFFFF, 0.0f, 1.0f,
		32.0f, -32.0f, 0x00FFFF00, 1.0f, 1.0f,
		-32.0f, 32.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(32.0f, -32.0f, 0x00FFFFFF, 1.0f, 1.0f,
		32.0f, 32.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-32.0f, 32.0f, 0x00FFFFFF, 0.0f, 0.0f);

	PlayerMesh = AEGfxMeshEnd();

	//TODO: Convert Ability mesh to 20x20 and draw one per maxhp

	AEGfxMeshStart();
	AEGfxTriAdd(-100.0f, -20.0f, 0x00FFFFFF, 0.0f, 1.0f,
		100.0f, -20.0f, 0x00FFFF00, 1.0f, 1.0f,
		-100.0f, 20.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(100.0f, -20.0f, 0x00FFFFFF, 1.0f, 1.0f,
		100.0f, 20.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-100.0f, 20.0f, 0x00FFFFFF, 0.0f, 0.0f);

	SlotBarMesh = AEGfxMeshEnd();

	AEGfxMeshStart();
	AEGfxTriAdd(-18.0f, -18.0f, 0x00FFFFFF, 0.0f, 1.0f,
		18.0f, -18.0f, 0x00FFFF00, 1.0f, 1.0f,
		-18.0f, 18.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(18.0f, -18.0f, 0x00FFFFFF, 1.0f, 1.0f,
		18.0f, 18.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-18.0f, 18.0f, 0x00FFFFFF, 0.0f, 0.0f);

	ResourceMesh = AEGfxMeshEnd();

	AEGfxMeshStart();
	AEGfxTriAdd(-12.0f, -12.0f, 0x00FFFFFF, 0.0f, 1.0f,
		12.0f, -12.0f, 0x00FFFF00, 1.0f, 1.0f,
		-12.0f, 12.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(12.0f, -12.0f, 0x00FFFFFF, 1.0f, 1.0f,
		12.0f, 12.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-12.0f, 12.0f, 0x00FFFFFF, 0.0f, 0.0f);

	ResourceMeshHalf = AEGfxMeshEnd();

	AEGfxMeshStart();
	AEGfxTriAdd(-6.0f, -6.0f, 0x00FFFFFF, 0.0f, 1.0f,
		6.0f, -6.0f, 0x00FFFF00, 1.0f, 1.0f,
		-6.0f, 6.0f, 0x0000FFFF, 0.0f, 0.0f);

	AEGfxTriAdd(6.0f, -6.0f, 0x00FFFFFF, 1.0f, 1.0f,
		6.0f, 6.0f, 0x00FFFFFF, 1.0f, 0.0f,
		-6.0f, 6.0f, 0x00FFFFFF, 0.0f, 0.0f);

	ResourceMeshThird = AEGfxMeshEnd();

	PlayerTexture = AEGfxTextureLoad("..\\Assets\\Character.png");
	AbilityBar = AEGfxTextureLoad("..\\Assets\\UI_AbilitySlot.png");
	HealthIcon = AEGfxTextureLoad("..\\Assets\\UI_Heart.png");
	EnergyIcon = AEGfxTextureLoad("..\\Assets\\UI_Heart.png");
}

void Player::EnergyBarUpdate()
{
	++ticks;
	// energy system
	if (ticks > 120)
	{
		if (energy < maxEnergy)
		{
			if (energy + 1 >= maxEnergy)
				energy = maxEnergy;
			else
				energy += 1;
			ticks = 0;
		}
	}

	/*
	if (PlayerUseSkill(skillSlots[sampleNum]) == true)
	{
		if (skillSlots->Cooldown == 0)
		{
			if ((energy - skillSlots->cost) >= 0)
				energy = 0;
			else
			{
				// Issue in game message "Not Enough Energy!"
			}
		}
	}
	*/
}

//movement input
void Player::Update(float dt)
{

	if (hp < 1)
		isActive = false;
	if (isActive)
	{
		// movement input
		if (AEInputCheckCurr(AEVK_UP) || AEInputCheckCurr(AEVK_W))
		{
			positionY += (velocityY * spd) * dt * speedmod;
		}

		else if (AEInputCheckCurr(AEVK_DOWN) || AEInputCheckCurr(AEVK_S))
		{
			positionY -= (velocityY * spd) * dt * speedmod;
		}

		if (AEInputCheckCurr(AEVK_RIGHT) || AEInputCheckCurr(AEVK_D))
		{
			positionX += (velocityX * spd) * dt * speedmod;
		}

		else if (AEInputCheckCurr(AEVK_LEFT) || AEInputCheckCurr(AEVK_A))
		{
			positionX -= (velocityX * spd) * dt * speedmod;
		}

		if (AEInputCheckCurr(AEVK_SPACE))
		{
			//Ability code here?
			energy = 0;
		}

		minmaxUpdate();
		EnergyBarUpdate();
	}

}

void Player::Draw()
{
	if (hp < 1)
		isActive = false;
	if (isActive)
	{
		// character render
		AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
		AEGfxSetBlendMode(AE_GFX_BM_BLEND);
		AEGfxSetPosition(positionX, positionY);
		AEGfxTextureSet(PlayerTexture, 0.0f, 0.0f);
		AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
		AEGfxSetTransparency(1.0f);
		AEGfxMeshDraw(PlayerMesh, AE_GFX_MDM_TRIANGLES);
	}

	//Ability Render
	AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
	AEGfxSetBlendMode(AE_GFX_BM_BLEND);
	AEGfxSetPosition(450.0f, 300.0f);
	AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
	AEGfxSetTransparency(1.0f);
	AEGfxTextureSet(AbilityBar, 0.0f, 0.0f);
	AEGfxMeshDraw(SlotBarMesh, AE_GFX_MDM_TRIANGLES);

	//HP Render

	float x = -530.0f;
	float y = 300.0f;
	for (int i = 0; i < maxHP; ++i, x+= 40.0f)
	{
		AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
		AEGfxSetBlendMode(AE_GFX_BM_BLEND);
		AEGfxSetPosition(x, y);
		AEGfxTextureSet(HealthIcon, 0.0f, 0.0f);
		if (i < hp)
			AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);
		else
			AEGfxSetTintColor(0.5f, 1.0f, 0.5f, 0.8f);
		AEGfxSetTransparency(1.0f);
		AEGfxMeshDraw(ResourceMesh, AE_GFX_MDM_TRIANGLES);
	}

	//Energy Render

	x = -530.0f;
	y = 250.0f;
	for (int i = 0; i < maxEnergy / 3; ++i, x += 40.0f)
	{
		AEGfxSetRenderMode(AE_GFX_RM_TEXTURE);
		AEGfxSetBlendMode(AE_GFX_BM_BLEND);
		AEGfxSetPosition(x, y);
		AEGfxTextureSet(EnergyIcon, 0.0f, 0.0f);
		AEGfxSetTransparency(1.0f);
		if (i < (energy + 2) / 3)
		{
			AEGfxSetTintColor(0.0f, 1.0f, 0.3f, 1.0f);

			if (energy % 3 == 2 && i == energy / 3)
				AEGfxMeshDraw(ResourceMeshHalf, AE_GFX_MDM_TRIANGLES);
			else if (energy % 3 == 1 && i == energy / 3)
				AEGfxMeshDraw(ResourceMeshThird, AE_GFX_MDM_TRIANGLES);
			else
				AEGfxMeshDraw(ResourceMesh, AE_GFX_MDM_TRIANGLES);
		}
		else
		{
			AEGfxSetTintColor(0.1f, 0.1f, 0.1f, 1.0f);
			AEGfxMeshDraw(ResourceMesh, AE_GFX_MDM_TRIANGLES);
		}
	}
}