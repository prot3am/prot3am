#pragma once

enum NODETYPE
{
	WALKABLE = 0,
	NONWALKABLE,
	BURNING
};