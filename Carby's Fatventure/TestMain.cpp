#include "TestMain.h"

double dt;
double t;

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	AESysInit(hInstance, nCmdShow, 1280, 720, 1, 60, true, NULL);

	AESysSetWindowTitle("Carby's Fatventure");

	UNREFERENCED_PARAMETER(pCmdLine);
	UNREFERENCED_PARAMETER(hPrevInstance);

	// Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	//int * pi = new int;
	AESysReset();

	dt = 0.0;
	t = 0.0;
	double currTime = clock();

	const int wallNum = 8;
	float drawX = -500.0f;
	float drawY = -200.0f;

	AEGfxSetBackgroundColor(0.0f, 0.0f, 0.0f);

	GSM_Initialize(GS_TESTLEVEL);
	Player *carby = new Player(3, 5, 1, 2.0f, 0, 0, 15);
	Room *testRoom = new Room();
	carby->setPosition(testRoom->getNode(0, 0));
	walltiles testwalls[wallNum];
	floortiles testfloor;

	Enemy *temp = new Enemy(1, 3, 1, 1.0f, 2, 1, 3.0f, MELEE);
	temp->setPosition(testRoom->getNode(1, 1));
	testRoom->AddEnemy(temp);
	testRoom->SetEnemyActive(true);

	while (_current != GS_QUIT)
	{
		if (_current != GS_RESTART) // if level is the same as previous
		{
			GSM_Update(); // update game state type
			fpLoad();     // load all assets required for new game state
		}

		else
		{
			// retains same game state as previous
			_current = _previous;
			_next = _previous;
		}

		fpInit(); // initialize current game state

		//The game loop
		while (_current == _next)
		{
			double newTime = clock();
			dt += (newTime - currTime);
			if (dt * CLOCKS_PER_SEC)
			{
				currTime = newTime;
				AESysFrameStart();
				AEInputUpdate();
				fpDraw();
				AEVec2 prevPos = carby->getcurrPos();
				for (int counter = 0; counter < wallNum; ++counter)  // draws the walls and check collision
				{
					testwalls[counter].setPositionCoord(drawX, drawY);
					testwalls[counter].drawwallgraphics(drawX, drawY);
					drawY += 64.0f;
				}
				drawY = -200.0f; // reset for next iteration
				carby->Update((float)dt);
				testRoom->Update((float)dt, carby);
				for (int count = 0; count < wallNum; ++count)
				{
					if (carby->chkAABB(testwalls[count]) == true)
					{
						carby->setPositionCoord(prevPos.x, prevPos.y);
					}
				}

				//Draw
				testRoom->Draw();
				carby->Draw();

				fpUpdate(dt);
				_current = _next;
				AESysFrameEnd();

				//Reset Time
				t += dt;
				dt = 0;
			}
		}

		fpFree();

		if (_next != GS_RESTART)
			fpUnload();

		_previous = _current;

		_current = _next;
	}
	AESysExit();
}